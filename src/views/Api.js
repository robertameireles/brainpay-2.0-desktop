import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../components/Header'
import Footer from '../components/Footer'

const Api = () => {
    
    return (
        <>
        <Header/>
            <main className="container">
                <section className="container-text api">
                    <h1>API Documentation</h1>
                    <ul className="list-api">
                        <li className="link-api-collection">
                            <a href= "https://brain--app.s3.us-east-2.amazonaws.com/downloads/BrainPay_Postman_Collection.zip"><span className="download">Download the Postman Collection</span></a>
                        </li>
                        <p>
                            PREREQUISITES
                        </p>
                        <li>
                            <Link to='/api/createuser'><span>- Create a User</span></Link>
                        </li>
                        <li>
                            <Link to='/api/authenticateyouruserandgettoken'><span>- Authenticate Your User And Get a Token</span></Link>
                        </li>
                        <p>
                            KYC
                        </p>
                        <li>
                            <Link to='/api/registeruser'><span>- Register User</span></Link>
                        </li>
                        <li>
                            <Link to='/api/getregistereduserskyc'><span>- Get Registered Users KYC</span></Link>
                        </li>
                        <li>
                            <Link to='/api/getcep'><span>- Get CEP (Zip Code) </span></Link>
                        </li>
                        <p> 
                            RECHARGE METHODS
                        </p>
                        <li>
                            <Link to='/api/authentication'><span>- Authentication</span></Link>
                        </li>
                        <li>
                            <span>- Temporary Recharge Link</span>
                        </li>
                        <li>
                            <Link to='/api/onlinepix'><span className="padding-left-20px">- PIX</span></Link>
                        </li>
                        <li>
                            <Link to='/api/rechargeinformationretrieve'><span>- Recharge Information Retrieve</span></Link>
                        </li>
                        <li>
                            <Link to='/api/rechargecallback'><span>- Recharge Callback</span></Link>
                        </li>
                        <p>
                            WITHDRAWAL METHODS
                        </p>
                        <li>
                            <a href='https://swagger.brainpay.com.br/' target='_blank' rel="noopener noreferrer" ><span>- Online Swagger</span></a>
                        </li>
                        <li>
                            <Link to='/api/brazilianbanksretrieve'><span>- Brazilian Banks Retrieve</span></Link>
                        </li>
                        <li>
                            <Link to='/api/ted'><span>- TED</span></Link>
                        </li>
                        <li>
                            <Link to='/api/withdrawaltransactionretrieve'><span>- Withdrawal Transaction Retrieve</span></Link>
                        </li>
                        <li>
                            <Link to='/api/withdrawalcallback'><span>- Withdrawal Callback</span></Link>
                        </li>
                    </ul>
                </section>
            </main>
            <Footer check={false}/>
        </>
    )
}

export default Api
