import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import BoxBlackApi from '../../components/BoxBlackApi'
import Table from '../../components/Table'
import CodeBrazilianBanksRetrieve from '../../components/api/brazilianBanksRetrieve/TabsCodeBrazilianBanksRetrieve'

const BrazilianBanksRetrieve = () => {

    // Columns Table
    const columns = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
        ['Description', 'Description_Value']
    ]
    
    // Data Table
    // Header
    const data1 = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer token obtained from Authenticate Your User And Get a Token method. Ex: "Bearer <token>"'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]

    // Response fields (success)
    const data2 = [
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '200'},
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
        {Field_Velue: 'data', Required_Value: 'YES', Type_Value: 'Nested Field', Length_Value: '-', Description_Value: 'Brazilian banks'}
    ]


    // Black Box
    // Curl example
    const infobox1 = [
        {id:1, item: 'curl -X GET "https://service.brainpay.com.br/test/v1/withdrawal/banks" -H "accept:', classbox:'no-margin'}, 
        {id:2, item: 'application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
        {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3', classbox:'no-margin'},  
        {id:4, item: 'OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"', classbox:'no-margin'},  
    ]

    // Success Response
    const infobox2 = [
        {id:1, item: <span>&#91;</span>, classbox:'no-margin'},
        {id:2, item: <span>&#123;</span>, classbox:'padding-left-20px no-margin'},
        {id:3, item: '"value": "341",', classbox:'padding-left-20px no-margin'},  
        {id:4, item: '"label": "Itaú Unibanco SA"', classbox:'padding-left-20px no-margin'},  
        {id:5, item: <span>&#125;</span>, classbox:'no-margin padding-left-20px'},
        {id:6, item: <span>&#93;</span>, classbox:'no-margin'},
    ]


    // Erros
    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 409,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'message': 'An Error has occurred',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Error'", classbox:'padding-left-20px no-margin'},
        {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Brazilian Banks Retrieve</h1>

                <p className="padding-top-25px">Get the list of brazilian banks.</p>

                <p className="padding-bottom-25px padding-top-25px">Method: GET</p>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /test/v1/withdrawal/banks</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /prod/v1/withdrawal/banks</span></p>
                </div>

                <p>Header</p>
                < Table columns={columns} data={data1}/>
                <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'>Authenticate Your User And Get a Token.</Link></p>
                
                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns} data={data2}/>

                <p className="padding-top-25px">Curl example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p>Success Response</p>
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Errors</p>
                <BoxBlackApi infobox={infobox3}></BoxBlackApi>


                <CodeBrazilianBanksRetrieve></CodeBrazilianBanksRetrieve>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
            
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default BrazilianBanksRetrieve
