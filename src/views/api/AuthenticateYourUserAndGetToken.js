import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
import CodeGetToken from '../../components/api/getToken/TabsGetToken'


const AuthenticateYourUserAndGetToken = () => {
    
    // Columns Table
    const columns1 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Description', 'Description_Value'], 
    ];

    const columns2 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
    ];

    // Data Table

    // Header
    const data1 = [
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Description_Value: 'application/json'}
    ]

    // Payload
    const data2 = [
        {Field_Velue: 'email', Required_Value: 'YES', Type_Value: 'String', Length_Value: '50'},
        {Field_Velue: 'password', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100'},
    ]

    // Response fields (success)
    const data3 = [
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '200'},
        {Field_Velue: 'uniqueCode', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Company Unique Code'},
        {Field_Velue: 'expiresIn', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Token Expiration'},
        {Field_Velue: 'accessToken', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Token'},
    ]

    // Black Box
    // Request JSON payload example
    const infobox1 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: '"email": "johndoe@mycompany.com",', classbox:'padding-left-20px no-margin'},  
        {id:3, item: '"password": "12345678"', classbox:'padding-left-20px no-margin'},  
        {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Request curl example
    const infobox2 = [
        {id:1, item: <span>curl -X POST "https://service.brainpay.com.br/test/v1/auth/company/login" -H "accept: application/json"</span>, classbox:'no-margin'},
        {id:2, item: <span>-H "Content-Type: application/json" -d "&#123; \"email\": \"johndoe@mycompany.com\",</span>, classbox:'no-margin'},
        {id:3, item: <span>\"password\": \"12345678\"&#125;"</span>, classbox:'no-margin'},
    ]

    // Success response
    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'status': 'Success',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'code': 200,", classbox:'padding-left-20px  no-margin'}, 
        {id:4, item: "'uniqueCode': 'TEST',", classbox:'padding-left-20px no-margin'}, 
        {id:6, item: "'expiresIn': '2019-11-07 15:30:59',", classbox:'padding-left-20px no-margin'}, 
        {id:7, item: "'accessToken': 'dh7878.092832s_jhs093.kij8...'", classbox:'padding-left-20px no-margin'}, 
        {id:8, item: <span>&#125;</span>, classbox:'no-padding no-margin'},
    ]


    // Error
    const infobox4 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 404,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'message': 'Not allowed'", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    const infobox5 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'message': 'Email already registered',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'code': 401", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    const infobox6 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'message': 'An error has occurred',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Error',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'code': 409", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // A curl request would look like the following
    const infobox7 = [
        {id:1, item: 'curl -X POST "https://service.brainpay.com.br/test/v1/kyc/cpf" -H "accept:', classbox:'no-margin'}, 
        {id:2, item: 'application/json" -H "Content-Type: application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
        {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3', classbox:'no-margin'},  
        {id:4, item: 'OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"', classbox:'no-margin'},  
        {id:5, item: 'd "{ \\"id_card\\": \\"05998217608\\"}"', classbox:'no-margin'},  
    ]


    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">

            <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Authenticate Your User And Get a Token</h1>

                <div className="break-text-api">
                    <p>Once your user is activated, you are able to ask for a token to make requests to the other BrainPay services.
                    A token is always issued with a lifetime limit (currently, it is 15 minutes).</p>
                    <p>Every time your token expires, your client needs to ask for another by doing login with your user
                    and password created in the previous step.</p>
                </div>

                <p className="padding-bottom-5px">Method: POST</p>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong">/test/v1/auth/company/login </span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong">/prod/v1/auth/company/login</span></p>
                </div>

                <p>Header</p>
                <Table columns={columns1} data={data1}/>

                <p className="padding-top-25px">Payload</p>
                <Table columns={columns2} data={data2}/>

                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns1} data={data3}/>

                <p className="padding-top-25px">Request JSON payload example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p>Request curl example</p>
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Success response</p>
                <BoxBlackApi infobox={infobox3}></BoxBlackApi>

                <p>Errors</p>
                <BoxBlackApi infobox={infobox4}></BoxBlackApi>
                <BoxBlackApi infobox={infobox5}></BoxBlackApi>
                <BoxBlackApi infobox={infobox6}></BoxBlackApi>

                <h1 className="padding-top-25px">Do Your Requests With The Issued Token</h1>

                <div>
                    <p>Once you get your valid token, save it in your session and use it in your upcoming
                    requests.</p>
                    <p>The token should be always informed in the header section of your requests with the
                    following structure:</p>
                    <p>“Authorization: Bearer [token]”</p>
                    <p>The “Bearer” expression must be always there (case sensitive) and should keep a single space from the issued token.</p>
                </div>

                <div className="break-text-api">
                    <p className="blue">EXAMPLE</p> 
                    <div className="line-api"></div>
                    <p>Authorization: Bearer</p>
                    <p>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk1MzEzMTUsImlhdCI6MTU3OT</p>
                    <p>UzMDQxNSwic3ViIjozfQ.q-Jmn0j_41rxScTPKBrYxogPXGNHUehZT7tbgfioIWM"</p>
                </div>

                <p>A curl request would look like the following</p>
                <BoxBlackApi infobox={infobox7}></BoxBlackApi>

                <CodeGetToken></CodeGetToken>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default AuthenticateYourUserAndGetToken

