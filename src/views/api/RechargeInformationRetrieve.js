import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
// import ImgOne from '../../assets/api/recharge_one.png'
// import ImgTwo from '../../assets/api/recharge_two.png'
import CodeRechargeInformationRetrieve from '../../components/api/rechargeInformationRetrieve/TabRechargeInformationRetrieve'

const RechargeInformationRetrieve = () => {

    // Columns Table
    const columns1 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
        ['Description', 'Description_Value']
    ]

    const columns2 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
    ];

    // Data Table
    const data1 = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer <token>'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]

    // Payload
    const data2 = [
        {Field_Velue: 'data', Required_Value: 'YES', Type_Value: 'Collection', Length_Value: 'Until 100 transactions in a single request'},
    ]

    // Response fields (success)
    const data3 = [
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '200'},
        {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Request accepted'}
    ]
    
    // Black Box
    // Request JSON payload example
    const infobox1 = [
        {id:1, item: <span>&#123;</span> , classbox:'no-margin'},
        {id:2, item: '"data": ["6786136736361", "838791629616", "32696196343314"]', classbox:'padding-left-20px no-margin'},
        {id:3, item: <span>&#125;</span> , classbox:'no-margin'},
    ]

    // Request curl example
    const infobox2 = [
        {id:1, item: "curl -X 'POST' 'https://service.brainpay.com.br/test/v1/deposit/status'", classbox:'no-margin'}, 
        {id:2, item: "-H 'accept: application/json' -H 'Authorization: Baerer eyJ0eXAiOiJK", classbox:'no-margin'},  
        {id:3, item: "V1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjEwMTQ2MjcsImlhdCI6MTYyMTAxMzcyN", classbox:'no-margin'},  
        {id:4, item: "ywic3ViIjoxNTJ9.lEXgY8TT_fjOGfPkbiBbAtNbYxawx5snfuHrUGo27XM'", classbox:'no-margin'},  
        {id:5, item: "-H 'Content-Type: application/json'", classbox:'no-margin'}, 
        {id:6, item: "-d '{", classbox:'no-margin'}, 
        {id:6, item: '"data": [', classbox:'no-margin'}, 
        {id:6, item: '"/6786136736361/",', classbox:'no-margin'}, 
        {id:6, item: '"/838791629616/",', classbox:'no-margin'}, 
        {id:6, item: <span>"/32696196343314/"]&#125;&#39;'</span>, classbox:'no-margin'}, 
    ]

    // Approved response
    const infobox3 = [
        {id:1, item: <span>&#123;</span> , classbox:'no-margin'},
        {id:2, item: "'status': 'Success',", classbox:'padding-left-20px no-margin'},
        {id:3, item: "'code': 200,", classbox:'padding-left-20px no-margin'},
        {id:5, item: "'message': 'Request accepted'", classbox:'padding-left-20px no-margin'},
        {id:6, item: <span>&#125;</span> , classbox:'no-margin'},
    ]

    // Erro
    const infobox4 = [
        {id:1, item: <span>&#123;</span> , classbox:'no-margin'},
        {id:2, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'},
        {id:2, item: "'code': 406,", classbox:'padding-left-20px no-margin'},
        {id:4, item: "'message': 'Error description'", classbox:'padding-left-20px no-margin'},
        {id:9, item: <span>&#125;</span> , classbox:'no-margin'},
    ]
    
    const infobox5 = [
        {id:1, item: <span>&#123;</span> , classbox:'no-margin'},
        {id:2, item: "'status': 'Error',", classbox:'padding-left-20px no-margin'},
        {id:2, item: "'code': 409,", classbox:'padding-left-20px no-margin'},
        {id:4, item: "'message': 'An error has occurred'", classbox:'padding-left-20px no-margin'},
        {id:9, item: <span>&#125;</span> , classbox:'no-margin'},
    ]

    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p className="padding-top-20px"><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Recharge Information Retrieve</h1>

                <p className="padding-top-25px">This service provides the status of a recharge transaction.</p>
                <p>It is possible to check up to 100 transactions in a single request. </p>
                <p>The status of transactions will be sent by webhook.</p>
                
                <p className="padding-bottom-30px">The transaction_id field refers to the same value you got when requested in
                <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'> Authenticate Your User And Get Token</Link>.</p>
            
                <p>Method: POST</p>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p className="blue-no-strong">/test/v1/deposit/status</p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p className="blue-no-strong"> /prod/v1/deposit/status</p>
                </div>

                <p>Header</p>
                <Table columns={columns1} data={data1}/>
                <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'>Authenticate Your User And Get a Token.</Link></p>

                <p className="padding-top-25px">Payload</p>
                < Table columns={columns2} data={data2}/>

                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns1} data={data3}/>

                <p className="padding-top-25px">Request JSON payload example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p className="padding-top-25px">Request curl example</p>
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>  

                <p>Success response</p>
                <BoxBlackApi infobox={infobox3}></BoxBlackApi>

                <p>Error</p>
                <BoxBlackApi infobox={infobox4}></BoxBlackApi>
                <BoxBlackApi infobox={infobox5}></BoxBlackApi>

                <CodeRechargeInformationRetrieve></CodeRechargeInformationRetrieve>

                <p><Link to='/api'>Back to API Documentation</Link></p>
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default RechargeInformationRetrieve
