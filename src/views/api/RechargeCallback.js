import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'

const RechargeCallback = () => {
    
    // Columns Table
    const columns = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
        ['Description', 'Description_Value']
    ];

    // Data Table
    // Data fields
    const data = [
        {Field_Velue: 'transaction_id', Required_Value: 'YES', Type_Value: 'String', Length_Value: '36', Description_Value: 'The transaction ID number'},
        {Field_Velue: 'amount', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '15', Description_Value: 'Transaction amount (cents)'},
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '1', Description_Value: 'Status of the transaction (enum: 0=processing, 1=approved,2=denied)'},
        {Field_Velue: 'datetime', Required_Value: 'YES', Type_Value: 'Datetime', Length_Value: ' ', Description_Value: 'Date/time of the transaction'},
        {Field_Velue: 'signature*', Required_Value: 'YES', Type_Value: 'String', Length_Value: '32', Description_Value: 'MD5 transction signature'},
    ]

    // Black Box
    // Request example
    const infobox1 = [
        {id:1, item: <span>&#123;</span> , classbox:'no-margin'},
        {id:2, item:'transaction_id: 567616d4-f589-40b3-a87d-2319b2cf5a5c,',classbox:'padding-left-20px no-margin'},
        {id:3, item:'amount: 10002,',classbox:'padding-left-20px no-margin'},
        {id:4, item:'status: 2,',classbox:'padding-left-20px no-margin'},
        {id:5, item:'datetime: 2011-08-12T20:17:46.384Z,',classbox:'padding-left-20px no-margin'},
        {id:6, item:'signature: 0964e39b6a1309d4978b0c775423ebb5',classbox:'padding-left-20px no-margin'},
        {id:7, item: <span>&#125;</span> , classbox:'no-margin'},
    ]
    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Recharge Callback</h1>

                <p>Method: POST</p>
                
                <p className="padding-top-25px">This implementation is optional. If you prefer to be informed back when a transaction
                status changes, you can build a callback API as described below.</p>

                <p>This webhook updates the status of a transaction and may be provided by your side.</p>
                
                <p className="padding-top-25px">Data fields:</p>
                <Table columns={columns} data={data}/>

                <p className="padding-top-25px">Request example:</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>
                
                <h3>Signature field</h3>
                    
                <p>The signature field is an md5 hash based on a querystring-like format of the fields/values for
                the request with a secret key in common between the client and server of the webhook.</p>

                <p>In the request example above, the secret key value applied was “6!^6p6xnaz6O2RneYN^x”.
                So, the querystring of the request would be like:</p>
                
                <div className="break-text-api">
                    <p className="no-margin-bottom">transaction_id=567616d4-f589-40b3-a87d-2319b2cf5a5c&amp;amount=10.03&amp;status=1&amp;datetim</p>
                    <p className="no-margin">e=2020-11-27T17:31:43TZ&amp;key=6!^6p6xnaz6O2RneYN^x</p>
                </div>

                <p>Then, the md5 of the querytring would be:</p>

                <div className="break-text-api">
                    <p className="no-margin-bottom">md5(“transaction_id=567616d4-f589-40b3-a87d-2319b2cf5a5c&amp;amount=10.03&amp;status=1&amp;d</p>
                    <p className="no-margin">atetime=2020-11-27T17:31:43TZ&amp;key=6!^6p6xnaz6O2RneYN^x”) =</p>
                    <p className="no-margin">ef16d35e0320ecf430f25bbb05746280</p>
                </div>

                <p>The webhook expects three different status responses for the post. No json payload is
                needed for the response.</p>
                
                <div>
                    <p className="no-margin">1) status=201</p>
                    <p className="no-margin padding-left-20px">It means that everything was correctly processed:</p>
                    <p className="no-margin padding-left-40px">- the user id was found</p>
                    <p className="no-margin padding-left-40px">- the signature checked</p>
                </div>
                
                <div>
                    <p className="no-margin-bottom">2) status=401</p>
                    <p className="no-margin padding-left-20px">It means that the request was rejected because the signature didn’t check.</p>
                </div>

                <div>
                    <p className="no-margin-bottom ">3) status=404</p>
                    <p className="no-margin padding-left-20px">It means that the user id was not found.</p>
                </div>
                
                <div>
                    <p className="no-margin-bottom ">4) status=417</p>
                    <p className="no-margin padding-left-20px">It means that the request wasn’t processed due to another reason.</p>
                </div>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
              
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default RechargeCallback
