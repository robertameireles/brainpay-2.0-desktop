// import React from 'react'
// import { Link } from 'react-router-dom'
// import Header from '../../components/Header'
// import Footer from '../../components/Footer'
// import BoxBlackApi from '../../components/BoxBlackApi'
// import ImgOne from '../../assets/api/temporary_pix_one.png'
// import ImgTwo from '../../assets/api/temporary_pix_two.png'
// import ImgThree from '../../assets/api/temporary_pix_three.png'
// import ImgFour from '../../assets/api/temporary_pix_four.png'


// const OfflinePix = () => {
//         // Black Box
//         // Success Response:
//         const infobox2 = [
//             {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//             {id:2, item: '"code": 200,', classbox:'padding-left-20px no-margin'},  
//             {id:3, item: '"status": "Approved",', classbox:'padding-left-20px no-margin'}, 
//             {id:4, item: '"transaction_id": "08d82909-f5d6-4437-b621-79260ee62f15",', classbox:'padding-left-20px no-margin'}, 
//             {id:5, item: '"amount": "1200.00"', classbox:'padding-left-20px no-margin'}, 
//             {id:6, item: <span>&#125;</span>, classbox:'no-margin'},
//         ]

//         // Not Found:
//         const infobox3 = [
//             {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//             {id:2, item: '"status": "Not found",', classbox:'padding-left-20px no-margin'},  
//             {id:3, item: '"code": 404,', classbox:'padding-left-20px no-margin'}, 
//             {id:4, item: '"message": "Register not found"', classbox:'padding-left-20px no-margin'}, 
//             {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//         ]

//         // Unauthorized:
//         const infobox4 = [
//             {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//             {id:2, item: '"code": 401,', classbox:'padding-left-20px no-margin'},  
//             {id:3, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
//             {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
//         ]

//         // Internal server error:
//         const infobox5 = [
//             {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//             {id:2, item: '"code": 409,', classbox:'padding-left-20px no-margin'},  
//             {id:3, item: '"status": "string",', classbox:'padding-left-20px no-margin'},
//             {id:4, item: '"description": "string"', classbox:'padding-left-20px no-margin'}, 
//             {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//         ]

//         // Curl example:
//         const infobox1 = [
//             {id:1, item: <span>curl -X GET "http://sandbox.brainpay.com.br/recharge/4765759" -H "accept:</span>, classbox:'no-margin'},
//             {id:2, item: <span>application/json" -H "Authorization:</span>, classbox:'no-margin'},
//             {id:3, item: <span>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2</span>, classbox:'no-margin'},
//             {id:4, item: <span>MTQwNzY0MTQsImlhdCI6MTYxNDA3NTUxNCwic3ViIjozfQ.Uj</span>, classbox:'no-margin'},
//             {id:5, item: <span>yec6v1kuCTL2RTqD6b9MrGN24mi4L9csB4yjKRyWY"</span>, classbox:'no-margin'},
//         ]
//         return(
//             <>
//                 <Header/>
//                 <main className="container"> 
//                     <section className="container-text api">
//                         <p><Link to='/api'>Back to API Documentation</Link></p>
                        
//                         <h1>Temporary Recharge Link</h1>

//                         <p>If you want to use the Brain Pay interface, use our API as below:</p>
                        
//                         <div>
//                             <p>With the fresh token generated in the previous step <Link className="link-view-api" to='/api/authentication'> Authentication in Production Environment</Link>, you 
//                             have to take the user to the temporary link. In case of questions about generating token, refer to <Link className="link-view-api" to='/api/authentication'>Authentication in Production Environment.</Link></p>
//                         </div>
                        
//                         <div>
//                             <p>This link will be available by concatenating the address of the environment with the amount that the 
//                             user will pay and the token string generated in the previous step:</p>
//                         </div>

//                         <div className="break-text-api">
//                             <p className="blue">PRODUCTION</p> 
//                             <div className="line-api"></div>
//                             <p><span className="blue-no-strong">https://deposit.brainpay.com.br/pix/1/</span>&lt;amount&gt;&lt;token&gt;</p>
//                         </div>

//                         <div>
//                             <p>The Payment method "PIX" is ​​only available when using a fixed value on url (It has to be an integer value).
//                             This fixed value is the amount the user will pay.</p>
//                         </div>

//                         <div>
//                             <p>For instance, a temporary page generated from the amount of R$ 120 would have a like that look like this:</p>
//                         </div>

//                         <div className="break-text-api">
//                             <p>https://deposit.brainpay.com.br/pix/1/<span className="blue-no-strong">120/</span>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ</p>
//                             <p>leHAiOjE2MTEzNDA4MjcsImlhdCI6MTYxMTMzOTkyNywic3ViIjozMjI0NDN9.VKh6AMlooazsZR</p>
//                             <p>bCODH8lqQV-QmrkprZFN0ESp-0fVo</p>
//                         </div>

//                         <div>
//                             <p>This page has a lifetime of 15 minutes. After that, it no longer can be found.</p>
//                         </div>
                        
//                         <h3>Pages</h3>

//                         <p>The webpage generated by the link above would look like this:</p>

//                         <div className="padding-left-40px">
//                             <p> 1 - “Valor” (Amount) - Fixed value on url.</p>
//                             <p>2 - "Nome" (Enter Name) - Must be a registered user.</p>
//                             <p>3 - "CPF" (Enter CPF) - Must be a registered user.</p>
//                             <p>In case of questions about registering the user, refer to <Link className="link-view-api" to='/api/cpfqueryservice'>CPF Query Service</Link> </p>
//                         </div>

//                         <img className="padding-bottom-30px" src={ImgOne} alt='PIX Our Interface'/>

//                         <p>Upon clicking on "CONFIRMAR" the following screen will pop:</p>

//                         <div className="padding-left-40px">
//                             <p>The user will make a payment following the steps:</p>
//                             <p>1 - “Destinatário” (Receiver)-The receiver will be 37835641000176, company LY INTERMEDIACAO E COMERCIO DE A.</p>
//                             <p>2 - "Valor" (Amount)-Fixed value on url, but the user will put the amount with the cents. This will make your approval faster. </p>
//                             <p>3 - "Copie e cole na mensagem do PIX o número da transação:" - (Copy and paste the transaction number into the PIX message).
//                             The transaction ID number was generated at the same time when the token was <Link className="link-view-api" to='/api/authentication'>Authentication in Production Environment</Link>.</p>
//                         </div>

//                         <img className="padding-bottom-30px" src={ImgTwo} alt='PIX Our Interface'/>

//                         <p>Upon clicking on "ENVIAR COMPROVANTE" the following screen will pop:</p>

//                         <div className="padding-left-40px">
//                             <p>1 - The user will send the proof of payment.</p>
//                         </div>

//                         <img className="padding-bottom-30px" src={ImgThree} alt='PIX Our Interface'/>

//                         <p>Upon clicking on "ENVIAR" the following screen will pop:</p>

//                         <div className="padding-left-40px">
//                             <p>1 - This message means the payment was successfully made.</p>
//                         </div>

//                         <img className="padding-bottom-30px" src={ImgFour} alt='PIX Our Interface'/>

//                         <h3>Retrieve a recharge status</h3>

//                         <div>
//                             <p className="strong">"Important: In order to avoid flooding our server with many requests from several companies, we strongly 
//                             recommend not using this method as your main resource for getting a transaction status. Instead, use our Callback Methods."</p>
//                         </div>

//                         <p>You can see it whit the trasaction ID number generated in step <Link className="link-view-api" to='/api/authentication'>Authentication in Production Environment.</Link></p>

//                         <div className="break-text-api">
//                             <p className="blue">BRAINPAY API</p> 
//                             <div className="line-api"></div>
//                             <p>http://sandbox.brainpay.com.br/recharge/<span className="blue">transaction_id</span></p>
//                         </div>

//                         <div className="break-text-api">
//                             <p className="blue">EXAMPLE</p> 
//                             <div className="line-api"></div>
//                             <p>Example: http://sandbox.brainpay.com.br/recharge/<span className="blue">4765759</span></p>
//                         </div>

//                         <p>Curl example:</p>
//                         <BoxBlackApi infobox={infobox1}></BoxBlackApi>
            
//                         <p>Success Response:</p>
//                         <BoxBlackApi infobox={infobox2}></BoxBlackApi>

//                         <p>Not found (user not found):</p>
//                         <BoxBlackApi infobox={infobox3}></BoxBlackApi>

//                         <p>Errors:</p>

//                         <p>Unauthorized</p>
//                         <BoxBlackApi infobox={infobox4}></BoxBlackApi>

//                         <p>Internal server error</p>
//                         <BoxBlackApi infobox={infobox5}></BoxBlackApi>

//                         <p><Link to='/api'>Back to API Documentation</Link></p>
//                     </section>
//                 </main>
//                 <Footer check={false} />
//             </>
//         )
// }
// export default OfflinePix