// import React from 'react'
// import { Link } from 'react-router-dom'
// import Header from '../../components/Header'
// import Footer from '../../components/Footer'
// import Table from '../../components/Table'
// import BoxBlackApi from '../../components/BoxBlackApi'
// import WithdrawForm from '../../components/withdrawForm'

// const WithdrawalRequestService = () => {
    
//     //Columns Table
//     const columns1 = [
//         ['Form Example', 'Form_Example'],
//         ['Field', 'Field_Velue'], 
//         ['Required', 'Required_Value'],
//         ['Type', 'Type_Value'],
//         ['Length', 'Length_Value'],
//         ['Description', 'Description_Value']
//     ];

//     const columns2 = [
//         ['erroId', 'erroId_value'], 
//         ['description', 'description_value']
//     ];


//     // Data Table
//     // Data fields:
//     const data1 = [
//         {Form_Example: '1', Field_Velue: 'id_card', Required_Value: 'YES', Type_Value: 'String', Length_Value: '14', Description_Value: 'Identifies the ID number of user'},
//         {Form_Example: '2', Field_Velue: 'name', Required_Value: 'YES', Type_Value: 'String', Length_Value: '50', Description_Value: 'Identifies the name of customer'},
//         {Form_Example: '3', Field_Velue: 'amount', Required_Value: 'YES', Type_Value: 'String', Length_Value: '15', Description_Value: 'Transfer amount (cents)'},
//         {Form_Example: '4', Field_Velue: 'bank_number', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: 'Bank ID (see List Brazilian Banks service below)'},
//         {Form_Example: '5', Field_Velue: 'branch', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '4', Description_Value: 'Bank branch number'},
//         {Form_Example: '6', Field_Velue: 'account_number', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '20', Description_Value: 'Recipient account number'},
//         {Form_Example: '7', Field_Velue: 'digit_code', Required_Value: 'YES', Type_Value: 'String', Length_Value: '2', Description_Value: 'Recipient account number digit code'},
//         {Form_Example: '8', Field_Velue: 'account_type', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '1', Description_Value: '1​ for “Conta Corrente” (checking account), 2​ for “Conta Poupança” (savings account)'},
//         {Form_Example: '-', Field_Velue: 'transfer_type', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '1', Description_Value: '1​ (TED)'},
//     ]
    
//     // Error list:
//     const data2 = [
//         {erroId_value: '276', description_value: 'Communication with the bank failed'},
//         {erroId_value: '688', description_value: 'Bank was not found'},
//         {erroId_value: '690', description_value: 'Account number does not exist'},
//         {erroId_value: '691', description_value: 'CPF of the bank account is invalid'},
//         {erroId_value: '692', description_value: 'Bank code is invalid'},
//         {erroId_value: '693', description_value: 'Branch number is invalid'},
//         {erroId_value: '694', description_value: 'Account number is invalid'},
//         {erroId_value: '701', description_value: 'An error has occurred during the transaction. Please, try again.'},
//     ]

//     // Black Box
//     // Request JSON payload example:
//     const infobox1 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item:'"same_customer": true,',classbox:'padding-left-20px no-margin'},
//         {id:3, item:'"id_card": "string",',classbox:'padding-left-20px no-margin'},
//         {id:4, item:'"data": [',classbox:'padding-left-20px no-margin'},
//         {id:5, item: <span>&#123;</span>, classbox:'padding-left-40px no-margin'},
//         {id:6, item:'"id_card": "string",', classbox:'padding-left-60px no-margin'},
//         {id:7, item:'"name": "string",', classbox:'padding-left-60px no-margin'},
//         {id:8, item:'"amount": "10000",', classbox:'padding-left-60px no-margin'},
//         {id:9, item:'"bank_number": 0,', classbox:'padding-left-60px no-margin'},
//         {id:10, item:'"branch": 0,', classbox:'padding-left-60px no-margin'},
//         {id:11, item:'"account_number": 0,', classbox:'padding-left-60px no-margin'},
//         {id:12, item:'“digit_code”: “string”,', classbox:'padding-left-60px no-margin'},
//         {id:13, item:'"account_type": 0,', classbox:'padding-left-60px no-margin'},
//         {id:14, item:'"transfer_type": 0', classbox:'padding-left-60px no-margin'},
//         {id:15, item: <span>&#125;</span>, classbox:'padding-left-40px no-margin'},
//         {id:16, item: <span>&#93;</span>, classbox:'padding-left-20px no-margin'},
//         {id:17, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Success Response:
//     const infobox2 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 201,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"message": "Operation completed successfully",', classbox:'padding-left-20px no-margin'}, 
//         {id:4, item: '“transaction_id”: “123456”,', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: '“next_liquidation”: “Today”,', classbox:'padding-left-20px no-margin'}, 
//         {id:6, item: '"status": "Success",', classbox:'padding-left-20px no-margin'}, 
//         {id:7, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Unauthorized
//     const infobox3 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 401,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
//         {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Scarce
//     const infobox4 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 403,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"message": "Resource in scarse",', classbox:'padding-left-20px no-margin'},
//         {id:4, item: '"status": "Scarse balance",', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Not Found
//     const infobox5 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 404,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"message": "Register not found",', classbox:'padding-left-20px no-margin'},
//         {id:4, item: '"status": "Not Found",', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Not Found
//     const infobox6 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 412,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"message": "Amount is too low",', classbox:'padding-left-20px no-margin'},
//         {id:4, item: '"status": "Too low",', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Internal server error
//     const infobox7 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 409,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: '"message": "An error has occured",', classbox:'padding-left-20px no-margin'},
//         {id:4, item: '"status": "Error",', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Curl example:
//     const infobox8 = [
//         {id:1, item: <span>curl -X POST "https://sandbox.brainpay.com.br/withdrawal/request" -H "accept:</span>, classbox:'no-margin'},
//         {id:2, item: <span>application/json" -H "Content-Type: application/json" -d "&#123; \"same_customer\": true,</span>, classbox:'no-margin'},
//         {id:3, item: <span>\"id_card\": \"string\", \"data\": [ &#123; \"id_card\": \"string\", \"name\": \"string\", \"amount\":</span>, classbox:'no-margin'},
//         {id:4, item: <span>\"10000\", \"bank_number\": 0, \"branch\": 0, \"account_number\":0, \”​ digit_code\”: \”string\”,</span>, classbox:'no-margin'},
//         {id:5, item: <span>\"account_type\": 0, \"transfer_type\": 0 &#125; ]&#125;" -H "Authorization: Bearer</span>, classbox:'no-margin'},
//         {id:6, item: <span>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk1MzEzMTUsImlhdCI6MTU3</span>, classbox:'no-margin'},
//         {id:7, item: <span>OTUzMDQxNSwic3ViIjozfQ.q-Jmn0j_41rxScTPKBrYxogPXGNHUehZT7tbgfioIWM"</span>, classbox:'no-margin'},
//     ]

//     // Unavailable
//     const infobox9 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"code": 417,', classbox:'padding-left-20px no-margin'},  
//         {id:3, item: <span>"message": "Couldn't freeze balance",</span>, classbox:'padding-left-20px no-margin'},
//         {id:4, item: '"status": "Freezing Error",', classbox:'padding-left-20px no-margin'}, 
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]
//     return (
//         <>
//         <Header/>
//         <main className="container"> 
//             <section className="container-text api">
//                 <p><Link to='/api'>Back to API Documentation</Link></p>

//                 <h1 className="padding-top-25px">Withdrawal Request Servicer</h1>

//                 <p>This service is responsible for transferring credits from accounts.</p>

//                 <p className="strong">Important: Prior to making a withdrawal request, it’s mandatory that the user’s cpf
//                 had been requested to the CPF Query Service API.</p>

//                 <div className="break-text-api">
//                     <p className="blue">SANDBOX</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> https://sandbox.brainpay.com.br/withdrawal/request</span></p>
//                 </div>

//                 <div className="break-text-api">
//                     <p className="blue">PRODUCTION</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> https://api.brainpay.com.br/withdrawal/request</span></p>
//                 </div>

//                 <p className="padding-bottom-25px">Method: POST</p>

//                 <p className="padding-top-25px">Data fields:</p>
//                 <Table columns={columns1} data={data1}/>

//                 <p className="no-margin-top">*bank_number can be obtained in the next session</p>

//                 <WithdrawForm/>
                
//                 <p>Error list:</p>
//                 <Table columns={columns2} data={data2}/>

//                 <p className="padding-top-25px">Request JSON payload example:</p>
//                 <BoxBlackApi infobox={infobox1}></BoxBlackApi>

//                 <p>Curl example:</p>
//                 <BoxBlackApi infobox={infobox8}></BoxBlackApi>

//                 <p>Success Response:</p>
//                 <BoxBlackApi infobox={infobox2}></BoxBlackApi>

//                 <p>Errors:</p>

//                 <p>Unauthorized</p>
//                 <BoxBlackApi infobox={infobox3}></BoxBlackApi>

//                 <p>Scarce</p>
//                 <BoxBlackApi infobox={infobox4}></BoxBlackApi>

//                 <p>Not Found</p>
//                 <BoxBlackApi infobox={infobox5}></BoxBlackApi>

//                 <p>Too low amount</p>
//                 <BoxBlackApi infobox={infobox6}></BoxBlackApi>

//                 <p>Internal server error</p>
//                 <BoxBlackApi infobox={infobox7}></BoxBlackApi>

//                 <p>Unavailable</p>
//                 <BoxBlackApi infobox={infobox9}></BoxBlackApi>

//                 <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
              
//             </section>
//         </main>
//         <Footer check={false}/>
//         </>
//     )
// }

// export default WithdrawalRequestService
