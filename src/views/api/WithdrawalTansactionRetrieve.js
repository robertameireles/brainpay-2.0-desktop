import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
import CodeWithdrawalTransactionRetriever from '../../components/api/withdrawalTransactionRetrieve/TabsWithdrawalTransactionRetrieve'

const WithdrawalTransactionRetrieve = () => {

    //Columns Table
    const columns = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
        ['Description', 'Description_Value']
    ]
    
    // Data Table
    // Header
    const data = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer token obtained from Authenticate Your User And Get a Token method. Ex: "Bearer <token>"'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]

    
    // Black Box
    // Curl example
    const infobox1 = [
        {id:1, item: 'curl -X GET "http://withdrawal.test.brainpay.com.br/withdrawal/retrieve/12345678" -H', classbox:'no-margin'}, 
        {id:2, item: '"accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
        {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk1MzEzMTUsImlhdCI6MTU3', classbox:'no-margin'},  
        {id:4, item: 'OTUzMDQxNSwic3ViIjozfQ.q-Jmn0j_41rxScTPKBrYxogPXGNHUehZT7tbgfioIWM"', classbox:'no-margin'},  
    ]

    // Success Response
    const infobox2 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: '"code": 201,', classbox:'padding-left-20px no-margin'},  
        {id:3, item: '"created_date": "2020-03-10T12:24:01",', classbox:'padding-left-20px no-margin'}, 
        {id:4, item: '“transaction_id”: “12345678”,', classbox:'padding-left-20px no-margin'}, 
        {id:5, item: '“error_code”: “001”,', classbox:'padding-left-20px no-margin'}, 
        {id:6, item: '"message": "SUCESSO",', classbox:'padding-left-20px no-margin'}, 
        {id:7, item: '"status": "Success",', classbox:'padding-left-20px no-margin'},
        {id:8, item: '"complete": "true/false",', classbox:'padding-left-20px no-margin'},
        {id:9, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Unauthorized
    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: '"code": 401,', classbox:'padding-left-20px no-margin'},  
        {id:3, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
        {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Not Found
    const infobox4 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: '"code": 404,', classbox:'padding-left-20px no-margin'},  
        {id:3, item: '"message": "Register not found",', classbox:'padding-left-20px no-margin'}, 
        {id:4, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]
    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Withdrawal Transaction Retrieve</h1>

                <p className="padding-top-25px">This service is used to retrieve information of a single transaction.</p>

                <p className="padding-top-25px">Method: GET</p>

                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong">  https://sandbox.brainpay.com.br/withdrawal/retrieve/&lt;transaction_id&gt;</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://api.brainpay.com.br/withdrawal/retrieve/&lt;transaction_id&gt;</span></p>
                </div>

                <p>Header</p>
                <Table columns={columns} data={data}/>
                <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'> Authenticate Your User And Get a Token.</Link></p>

                <p className="padding-top-25px">Curl example:</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p>Success Response:</p>
                <p>Note: when the value "complete" is set to true, it means that the transction reached its final status.</p>
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Errors:</p>
                
                <p>Unauthorized</p>
                <BoxBlackApi infobox={infobox3}></BoxBlackApi>

                <p>Not Found</p>
                <BoxBlackApi infobox={infobox4}></BoxBlackApi>

                <CodeWithdrawalTransactionRetriever></CodeWithdrawalTransactionRetriever>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
              
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default WithdrawalTransactionRetrieve
