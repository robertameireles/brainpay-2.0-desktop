import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
import CodeGetCep from '../../components/api/getCep/TabsGetCep'


const GetCep = () => {
    
    // Columns Table
    const columns = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Description', 'Description_Value'], 
    ];

    // Data Table
    // Header
    const data1 = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer token obtained from Authenticate Your User And Get a Token method. Ex: "Bearer <token>"'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]


    // Request query string
    const data2 = [
        {Field_Velue: '<zipNumber>', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11', Description_Value: 'Zip Code'},
    ]

    // Response fields (success) / User previously approved
    const data3 = [
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '200'},
        {Field_Velue: 'address', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Address'},
        {Field_Velue: 'zip', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11 fields', Description_Value: 'Zip Code'},
        {Field_Velue: 'street', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11 fields', Description_Value: 'Street'},
        {Field_Velue: 'district', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11 fields', Description_Value: 'District'},
        {Field_Velue: 'cityState', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11 fields', Description_Value: 'City State'},
    ]

    // Black Box
    // Request curl example
    const infobox1 = [
        {id:1, item: 'curl -X GET "https://service.brainpay.com.br/test/v1/kyc/cep/69985970" -H "accept:', classbox:'no-margin'}, 
        {id:2, item: 'application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
        {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3', classbox:'no-margin'},  
        {id:4, item: 'OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"', classbox:'no-margin'},  
    ]

    // Success response
    const infobox2 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: '"status": "success",', classbox:'padding-left-20px no-margin'},  
        {id:3, item: '"code": 200,', classbox:'padding-left-20px no-margin'}, 
        {id:4, item: '"address": {', classbox:'padding-left-20px no-margin'}, 
        {id:5, item: '"zip": "05730170",', classbox:'padding-left-40px no-margin'}, 
        {id:6, item: '"street": "Rua Maria José da Conceição",', classbox:'padding-left-40px no-margin'}, 
        {id:7, item: 'district": "Vila Andrade",', classbox:'padding-left-40px no-margin'}, 
        {id:8, item: '"cityState": "São Paulo/SP"', classbox:'padding-left-40px no-margin'}, 
        {id:9, item: <span>&#125;</span>, classbox:'padding-left-20px no-margin'},
        {id:9, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Error:
    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'code': 404,'", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'message': 'Invalid zip number',", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    const infobox4 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 409,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'message': 'An Error has occurred',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'status': 'Error',", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Get CEP (Zip Code)</h1>

                <p className="padding-top-25px">The CEP (Zip Code) is a postal code for Brazil. It identifies all the details of the address.</p>

                <p className="padding-top-25px">Method: GET</p>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>
                
                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /test/v1/kyc/cep/&#60;zipNumber&#62;</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /prod/v1/kyc/cep/&#60;zipNumber&#62;</span></p>
                </div>

                <p>Header</p>
                <Table columns={columns} data={data1}/>
                <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'>Authenticate Your User And Get a Token.</Link></p>

                <p className="padding-top-25px">Query String</p>
                <Table columns={columns} data={data2}/>

                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns} data={data3}/>

                <p className="padding-top-25px">Request curl example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>  

                <p>Success response</p>   
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Errors</p>

                <BoxBlackApi infobox={infobox3}></BoxBlackApi>
                <BoxBlackApi infobox={infobox4}></BoxBlackApi>
                
                <CodeGetCep></CodeGetCep>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default GetCep