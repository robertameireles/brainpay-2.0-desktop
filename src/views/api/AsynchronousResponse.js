// import React from 'react'
// import { Link } from 'react-router-dom'
// import Header from '../../components/Header'
// import Footer from '../../components/Footer'
// import Table from '../../components/Table'
// import BoxBlackApi from '../../components/BoxBlackApi'
// import CodeKycUser from '../../components/api/kyc/TabsKyc'

// const AsynchronousResponse = () => {
    
//     // Columns Table
//     const columns = [
//         ['Field', 'Field_Velue'], 
//         ['Required', 'Required_Value'],
//         ['Type', 'Type_Value'],
//         ['Length', 'Length_Value'],
//         ['Description', 'Description_Value']
//     ];

//     // Data Table
//     // Payload:
//     const data1 = [
//         {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer token obtained from Authenticate Your User And Get a Token method. Ex: "Bearer <token>"'},
//         {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
//     ]
//     // Request query string:
//     const data2 = [
//         {Field_Velue: '<idCard>', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11', Description_Value: 'User’s CPF number'},
//     ]

//     // Response fields (success):
//     // 
//     const data3 = [
//         {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
//         {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '201'},
//         {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'User previously approved'},
//         {Field_Velue: 'data', Required_Value: 'YES', Type_Value: 'Nested Field', Length_Value: '11 fields', Description_Value: ' '},
//     ]

//     const data4 = [
//         {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
//         {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '201'},
//         {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'CPF successfully recorded'},
//     ]

//     // Black Box
//     // Request JSON payload example:
//     const infobox1 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item:'"idCard": "05998217608"',classbox:'padding-left-20px no-margin'},
//         {id:8, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Curl example:
//     const infobox2 = [
//         {id:1, item: 'curl -X POST "https://service.brainpay.com.br/test/v1/kyc/cpf" -H "accept:', classbox:'no-margin'}, 
//         {id:2, item: 'application/json" -H "Content-Type: application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
//         {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3', classbox:'no-margin'},  
//         {id:4, item: 'OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"', classbox:'no-margin'},  
//     ]

//     // Success response example:
//     const infobox3 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item:'"status": "Success",',classbox:'padding-left-20px no-margin'},
//         {id:3, item:'"code": 201,',classbox:'padding-left-20px no-margin'},
//         {id:4, item:'"message": "User previously approved",',classbox:'padding-left-20px no-margin'},
//         {id:5, item:'"data": {', classbox:'padding-left-20px no-margin'},
//         {id:6, item:'"name": "SILVANIA FREIRE ARAUJO"', classbox:'padding-left-40px no-margin'},
//         {id:7, item: <span>&#125;</span>, classbox:'padding-left-20px no-margin'},
//         {id:8, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     const infobox4 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"status": "Success",', classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: '"code": 201,', classbox:'padding-left-20px no-margin'},  
//         {id:4, item: '"message": "Request accepted"', classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // Erro:
//     const infobox5 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"status": "Invalid",', classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: '"code": 406,', classbox:'padding-left-20px no-margin'},  
//         {id:4, item: '"message": "Invalid CPF number"', classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     const infobox6 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: '"message": "Erro Message",', classbox:'padding-left-20px no-margin'},  
//         {id:4, item: '"code": 401"', classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     //TODO: CHECK THIS ITEM WITH EDSON, 
//     // DOES THIS ITEM EXIST IN THE NEW DOCUMENTATION?
//     // Internal Server Error:
//     // 502 - Query error
//     const infobox7 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:3, item: '"status": "Bad gateway",', classbox:'padding-left-20px no-margin'},  
//         {id:2, item: '"code": 502,', classbox:'padding-left-20px no-margin'}, 
//         {id:4, item: '"message": "string"', classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     return (
//         <>
//         <Header/>
//         <main className="container"> 
//             <section className="container-text api">
//                 <p><Link to='/api'>Back to API Documentation</Link></p>

//                 <h1 className="padding-top-25px">Asynchronous Response</h1>            
                
//                 <div className="break-text-api">
//                     <p>CPF is the Brazilian ID card for citizens. It’s an obligatory, unique number.</p>
//                     <p>The user registration is done even if the CPF is not in Brazil Federal Government database. 
//                     We receive the CPF, register it with general information, and later we register it according to 
//                     Brazil Federal Government data.</p>
//                 </div>

//                 <p className="padding-bottom-25px">Method: POST</p>

//                 <div className="break-text-api">
//                     <p><span className="strong">Important:</span> We also offer a synchronous API that communicates with the Brazil Federal Government database.</p>
//                 </div>

//                 <div className="break-text-api">
//                     <p className="blue">SERVERS</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
//                 </div>
    
//                 <div className="break-text-api">
//                     <p className="blue">SANDBOX</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> /test/v1/kyc/cpf</span></p>
//                 </div>

//                 <div className="break-text-api">
//                     <p className="blue">PRODUCTION</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> /prod/v1/kyc/cpf</span></p>
//                 </div>

//                 <p>Header</p>
//                 <Table columns={columns} data={data1}/>
//                 <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'>Authenticate Your User And Get a Token.</Link></p>
                
//                 <p className="padding-top-25px">Payload</p>
//                 <Table columns={columns} data={data1}/>

//                 <p className="padding-top-25px">Response fields (success):</p>
//                 <Table columns={columns} data={data2}/>

//                 <p className="padding-top-25px">Response fields (success) / User previously approved</p>
//                 <Table columns={columns} data={data3}/>

//                 <p className="padding-top-25px">Response fields (success) / User not yet registered</p>
//                 <Table columns={columns} data={data4}/>

//                 <p className="padding-top-25px">Request JSON payload example:</p>
//                 <BoxBlackApi infobox={infobox1}></BoxBlackApi>

//                 <p className="padding-top-25px">Curl example</p>
//                 <BoxBlackApi infobox={infobox2}></BoxBlackApi>
                
//                 <p>Success response example</p>
//                 <p className="padding-top-25px">User previously approved</p>
//                 <BoxBlackApi infobox={infobox3}></BoxBlackApi>

//                 <p className="padding-top-25px">User not yet registered</p>
//                 <BoxBlackApi infobox={infobox4}></BoxBlackApi>

//                 <p>Errors</p>
//                 <BoxBlackApi infobox={infobox5}></BoxBlackApi>
//                 <BoxBlackApi infobox={infobox6}></BoxBlackApi>
                
//                 <p>502 - Query error</p>
//                 <BoxBlackApi infobox={infobox7}></BoxBlackApi>

//                 <CodeKycUser></CodeKycUser>
                
//                 <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
//             </section>
//         </main>
//         <Footer check={false}/>
//         </>
//     )
// }

// export default AsynchronousResponse

