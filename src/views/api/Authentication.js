import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
// import ImgOne from '../../assets/api/authentication_one.png'
// import ImgTwo from '../../assets/api/authentication_two.png'
import ImgThree from '../../assets/api/authentication_three.png'
import ImgFour from '../../assets/api/authentication_four.png'
import ImgFive from '../../assets/api/authentication_five.png'
import ImgSix from '../../assets/api/authentication_six.png'
import CodeAuthentication from '../../components/api/authentication/TabsAuthentication';

const Authentication = () => {
    // Columns Table
    const columns = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
        ['Description', 'Description_Value']
    ]

    // Header
    const data = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Basic base64(user:password)'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]
    
    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Recharge Methods</h1>

                <div>
                    <p>A BrainPay recharge transaction occurs in an exclusive web interface, where the 
                    user is required to fill in the needed information in order to proceed with the payment process.
                    </p>
                </div>

                <div>
                    <p>This web interface is generated dynamically by an API authentication service firstly accessed 
                    by your company.</p>
                </div>

                <div>
                    <p>This authentication is Basic, and should bring your user and password concatenated with a “:” 
                    and encoded with base64.</p>
                </div>
                <p>Let’s check how it works.</p>


                <h1 className="padding-top-25px">Authentication</h1>

                <p className="padding-top-25px">Method: POST</p>
  
                <div className="break-text-api">
                    <p className="blue">ENDPOINTS</p> 
                    <div className="line-api"></div>
                    <p>Sandbox: <span className="blue-no-strong">https://deposit-sandbox.brainpay.com.br/jwt/access/</span></p>
                    <p>Production: <span className="blue-no-strong">https://deposit.brainpay.com.br/jwt/access/</span></p>
                    <p className="no-padding">Header: “Authorization: Basic  &lt;your username:your password in base 64&gt;”</p>
                    <p className="no-padding">Body: The body of the authentication must have the following format:</p>
                </div>

                <div className="padding-bottom-15px padding-top-5px">
                    <p>&#123;</p>
                    <p><span className="red padding-left-20px">"user_id"​ :</span><span className="blue-no-strong">  &lt;user id card&gt;</span></p>
                    <p>&#125;</p>
                </div>

                <img className="padding-bottom-30px" src={ImgThree} alt="Body of the authentication"/>

                <div className="break-text-api">
                    <p>In the production environment, it’s mandatory to have your own user and password approved by BrainPay team (see step “Create a user” for more information).</p>
                </div>

                <div className="break-text-api">
                    <p>Once your user is approved, you will need this to authenticate in production. The method is the same used in the test environment, but now you have to replace 
                    the user and password by the ones you created at the “Create a user” step.</p>
                </div>

                <div className="break-text-api">
                    <p>So, for instance, if your username is “janedoe@mycompany.com” and your password is “my-strong-pass”, then when we apply base64 to the string “janedoe@mycompany.com:my-strong-pass” it will become:
                    base64(“janedoe@mycompany.com:my-strong-pass”) = amFuZWRvZUBteWNvbXBhbnkuY29tOm15LXN0cm9uZy1wYXNz</p>
                </div>

                <div className="break-text-api">
                    <p>Therefore, your Basic authentication header will be:</p>
                    <p className="strong">Authorization: Basic amFuZWRvZUBteWNvbXBhbnkuY29tOm15LXN0cm9uZy1wYXNz</p>
                </div>
                
                <p>Header</p>
                <Table columns={columns} data={data}/>

                <img className="padding-top-25px" src={ImgFour} alt="Body of the authentication"/>

                <h2 className="padding-top-25px">Response</h2>
                    
                <div>
                    <p>A successful request for the endpoint above will get a json response (status=201 Created) like this:</p>
                </div>
                    
                <img className="padding-bottom-30px" src={ImgFive} alt='Json response'/>
                    
                <img className="padding-bottom-30px" src={ImgSix} alt='Json response'/>

                <div>
                    <p>Where:</p>
                    <p><span className="strong">- token:</span> is the token value you will use to generate a temporary recharge link.</p>
                    <p><span className="strong">- transaction_id:</span> is the unique number to that transaction and will be used until the end of the operation.</p>
                    <p><span className="strong">- expires_in:</span> is the expiration date/time of the token.</p>
                    <p><span className="strong">- message:</span> is the friendly response for the status of the communication.</p>
                </div>        
                    
                <CodeAuthentication></CodeAuthentication>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default Authentication