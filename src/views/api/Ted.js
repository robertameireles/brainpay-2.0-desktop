import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
import CodeTed from '../../components/api/ted/TabsTed'

const Ted = () => {
    
    //Columns Table
    const columns1 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Description', 'Description_Value'], 
    ];

    const columns2 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
    ];

    // Data Table
    // Header
    const data1 = [
        {Field_Velue: 'Authorization', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1024', Description_Value: 'Bearer token obtained from Authenticate Your User And Get a Token method. Ex: "Bearer <token>"'},
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Length_Value: '-', Description_Value: 'application/json'}
    ]

    // Payload
    const data2 = [
        {Field_Velue: 'idCard', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11'},
        {Field_Velue: 'name', Required_Value: 'YES', Type_Value: 'String', Length_Value: '50'},
        {Field_Velue: 'amount', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '500000'},
        {Field_Velue: 'bankNumber', Required_Value: 'YES', Type_Value: 'String', Length_Value: '3'},
        {Field_Velue: 'branch', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '999999'},
        {Field_Velue: 'accountNumber', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '999999999'},
        {Field_Velue: 'digitCode', Required_Value: 'YES', Type_Value: 'String', Length_Value: '2'},
        {Field_Velue: 'accountType', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '2'},
    ]

    // Response fields (success)
    const data3 = [
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: '200'},
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: 'Success'},
        {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Request accepted'},
    ]
    

    // Black Box
    // Request JSON payload example
    const infobox1 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item:"'idCard': '39189183002',",classbox:'padding-left-20px no-margin'},
        {id:3, item:"'name': 'José da Silva',",classbox:'padding-left-20px no-margin'},
        {id:4, item:"'amount': 1000,",classbox:'padding-left-20px no-margin'},
        {id:5, item:"'bankNumber': '001',", classbox:'padding-left-20px no-margin'},
        {id:6, item:"'branch': 122,", classbox:'padding-left-20px no-margin'},
        {id:7, item:"'accountNumber': 03582885,", classbox:'padding-left-20px no-margin'},
        {id:8, item:"'digitCode': '05',", classbox:'padding-left-20px no-margin'},
        {id:9, item:"'accountType': 1",classbox:'padding-left-20px no-margin'},
        {id:10, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Curl example
    const infobox2 = [
        {id:1, item: <span>curl -X POST "https://service.brainpay.com.br/v1/withdrawal/ted" -H "accept:</span>, classbox:'no-margin'},
        {id:2, item: <span>application/json" -H "Content-Type: application/json" -d "&#123; \"idCard\": \"39189183002\",</span>, classbox:'no-margin'},
        {id:3, item: <span>\"name\": \"José da Silva\", \"amount\": 1000, \"bankNumber\": 001, \"branch\": 122, \"accountNumber\":</span>, classbox:'no-margin'},
        {id:4, item: <span>03582885", \"digitCode\": \"05\", \"accountType\": 1, -H "Authorization: Bearer</span>, classbox:'no-margin'},
        {id:6, item: <span>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk1MzEzMTUsImlhdCI6MTU3</span>, classbox:'no-margin'},
        {id:7, item: <span>OTUzMDQxNSwic3ViIjozfQ.q-Jmn0j_41rxScTPKBrYxogPXGNHUehZT7tbgfioIWM"</span>, classbox:'no-margin'},
    ]

    // Success Response:
    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 200,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status':'Success',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'message':'Request accepted'", classbox:'padding-left-20px no-margin'},
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Erro
    const infobox4 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'code': 406,", classbox:'padding-left-20px no-margin'}, 
        {id:3, item: "'message': 'Error description'", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
    ]


    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">TED</h1>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong">/v1/withdrawal/ted</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong">/prod/withdrawal/ted</span></p>
                </div>

                <p className="padding-bottom-25px">Method: POST</p>

                <p className="padding-top-25px">Header</p>
                < Table columns={columns1} data={data1}/>
                <p>* In case of questions about generating token, refer to: <Link className="link-pg-api" to='/api/authenticateyouruserandgettoken'>Authenticate Your User And Get a Token.</Link></p>
                
                <p className="padding-top-25px">Payload</p>
                <Table columns={columns2} data={data2}/>

                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns1} data={data3}/>


                <p className="padding-top-25px">Request JSON payload example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p>Curl example</p>
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Success Response</p>
                <BoxBlackApi infobox={infobox3}></BoxBlackApi>

                <p>Error</p>

                <BoxBlackApi infobox={infobox4}></BoxBlackApi>

                <CodeTed></CodeTed>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
              
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default Ted
