import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Table from '../../components/Table'
import BoxBlackApi from '../../components/BoxBlackApi'
import CodeCreateUser from '../../components/api/createUser/TabsCreateUser'

const CreateUser = () => {
    
    // Columns Table
    const columns1 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Description', 'Description_Value'], 
    ];

    const columns2 = [
        ['Field', 'Field_Velue'], 
        ['Required', 'Required_Value'],
        ['Type', 'Type_Value'],
        ['Length', 'Length_Value'],
    ];

    // Data Table

    const data1 = [
        {Field_Velue: 'Content-Type', Required_Value: 'YES', Type_Value: 'String', Description_Value: 'application/json'}
    ]

    // Payload
    const data2 = [
        {Field_Velue: 'email', Required_Value: 'YES', Type_Value: 'String', Length_Value: '50'},
        {Field_Velue: 'password', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100'},
    ]

    // Response fields (success)
    const data3 = [
        {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '200'},
        {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
        {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'Company\'s user successfully created'},
    ]

    // Black Box
    // Request JSON payload example
    const infobox1 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'email': 'johndoe@mycompany.com',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'password': '12345678'", classbox:'padding-left-20px no-margin'},  
        {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Success response
    const infobox2 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 200,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Success',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'message': 'Company’s user successfully created'", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Error

    const infobox3 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'code': 406,", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Fail'", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'message': 'Invalid e-mail address'", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    const infobox4 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'message': 'Email already registered',", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Fail',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'code': 401", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    const infobox5 = [
        {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
        {id:2, item: "'message': 'An error has occurred'", classbox:'padding-left-20px no-margin'},  
        {id:3, item: "'status': 'Error',", classbox:'padding-left-20px no-margin'}, 
        {id:4, item: "'code': 409", classbox:'padding-left-20px no-margin'}, 
        {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
    ]

    // Request curl example
    const infobox6 = [
        {id:1, item: <span>curl -X POST "https://service.brainpay.com.br/test/v1/auth/company/account" -H "accept:</span>, classbox:'no-margin'},
        {id:2, item: <span>application/json" -H "Content-Type: application/json" -d "&#123; \"email\":</span>, classbox:'no-margin'},
        {id:3, item: <span>\"johndoe@email.com\", \"password\": \"12345678\"&#125;"</span>, classbox:'no-margin'},
    ]

    return (
        <>
        <Header/>
        <main className="container"> 
            <section className="container-text api">
                <p><Link to='/api'>Back to API Documentation</Link></p>

                <h1 className="padding-top-25px">Create a User</h1>

                <p className="padding-top-25px">In order to make requests to the other services, you need first create a user. </p>
                <p className="no-padding-top">After successfully creating a user, ask a BrainPay staff member to activate your account.</p>

                <p className="padding-top-25px">Method: POST</p>

                <div className="break-text-api">
                    <p className="blue">SERVERS</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> https://service.brainpay.com.br</span></p>
                </div>
                
                <div className="break-text-api">
                    <p className="blue">SANDBOX</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /test/v1/auth/company/account</span></p>
                </div>

                <div className="break-text-api">
                    <p className="blue">PRODUCTION</p> 
                    <div className="line-api"></div>
                    <p><span className="blue-no-strong"> /prod/v1/auth/company/account</span></p>
                </div>

                <p>Header</p>
                <Table columns={columns1} data={data1}/>

                <p className="padding-top-25px">Payload</p>
                <Table columns={columns2} data={data2}/>

                <p className="padding-top-25px">Response fields (success)</p>
                <Table columns={columns1} data={data3}/>

                <p className="padding-top-25px">Request JSON payload example</p>
                <BoxBlackApi infobox={infobox1}></BoxBlackApi>

                <p>Request curl example</p>
                <BoxBlackApi infobox={infobox6}></BoxBlackApi>  

                <p>Success response</p>   
                <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                <p>Errors</p>

                <BoxBlackApi infobox={infobox3}></BoxBlackApi>
                <BoxBlackApi infobox={infobox4}></BoxBlackApi>
                <BoxBlackApi infobox={infobox5}></BoxBlackApi>

                <div className="highlight-api">
                    <p>Important: After successfully creating 
                    your new user, you need to contact a staff member and ask for 
                    your account approval prior to being able to authenticate and 
                    getting a token.</p>
                </div>
                
                <CodeCreateUser></CodeCreateUser>

                <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
            </section>
        </main>
        <Footer check={false}/>
        </>
    )
}

export default CreateUser

