// import React from 'react'
// import { Link } from 'react-router-dom'
// import Header from '../../components/Header'
// import Footer from '../../components/Footer'
// import Table from '../../components/Table'
// import BoxBlackApi from '../../components/BoxBlackApi'

// const RealtimeResponse = () => {
    
//     // Columns Table
//     const columns = [
//         ['Field', 'Field_Velue'], 
//         ['Required', 'Required_Value'],
//         ['Type', 'Type_Value'],
//         ['Length', 'Length_Value'],
//         ['Description', 'Description_Value']
//     ];

//     // Data Table
//     // Payload:
//     const data1 = [
//         {Field_Velue: '<cpf_number>', Required_Value: 'YES', Type_Value: 'String', Length_Value: '11', Description_Value: 'User’s CPF number'},
//     ]

//     // Response fields (success):
//     const data2 = [
//         {Field_Velue: 'status', Required_Value: 'YES', Type_Value: 'String', Length_Value: '20', Description_Value: 'Success'},
//         {Field_Velue: 'code', Required_Value: 'YES', Type_Value: 'Integer', Length_Value: '3', Description_Value: '201'},
//         {Field_Velue: 'message', Required_Value: 'YES', Type_Value: 'String', Length_Value: '100', Description_Value: 'CPF found successfully'},
//         {Field_Velue: 'data', Required_Value: 'YES', Type_Value: 'Nested Field', Length_Value: '11 fields', Description_Value: ' '},
//     ]

//     // Data fields:
//     const data3 = [
//         {Field_Velue: 'name', Required_Value: 'YES', Type_Value: 'String', Length_Value: '50', Description_Value: 'User’s complete name'},
//         {Field_Velue: 'interpol', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Has an Interpol register'},
//         {Field_Velue: 'ofac', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Has an OFAC register'},
//         {Field_Velue: 'death', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Has a death certificate'},
//         {Field_Velue: 'pep', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Is PEP'},
//         {Field_Velue: 'pep_relative', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Is a relative PEP'},
//         {Field_Velue: 'valid_id_card', Required_Value: 'YES', Type_Value: 'Boolean', Length_Value: '1', Description_Value: 'Has a valid CPF'},
//         {Field_Velue: 'birth_date', Required_Value: 'YES', Type_Value: 'Date', Length_Value: '10', Description_Value: 'Birth of date (YYYY-MM-DD)'},
//         {Field_Velue: 'mothers_name', Required_Value: 'NO', Type_Value: 'String', Length_Value: '50', Description_Value: 'User’s mother’s name'},
//         {Field_Velue: 'gender', Required_Value: 'YES', Type_Value: 'String', Length_Value: '1', Description_Value: 'M=Male, F=Female'},
//         {Field_Velue: 'rg', Required_Value: 'NO', Type_Value: 'String', Length_Value: '20', Description_Value: 'User’s ID card of the Brazilian state'},
//     ]

//     // Black Box
//     // Curl example:
//     const infobox1 = [
//         {id:1, item: 'curl -X GET "https://sandbox.brainpay.com.br/user/cpf-research/18622943801" -H "accept:', classbox:'no-margin'}, 
//         {id:2, item: 'application/json" -H "Content-Type: application/json" -H "Authorization: Bearer', classbox:'no-margin'},  
//         {id:3, item: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3', classbox:'no-margin'},  
//         {id:4, item: 'OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"', classbox:'no-margin'},  
//     ]

//     // Success response example:
//     const infobox2 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item:'"status": "success",',classbox:'padding-left-20px no-margin'},
//         {id:3, item:'"code": 201,',classbox:'padding-left-20px no-margin'},
//         {id:4, item:'"message": "CPF found successfully",',classbox:'padding-left-20px no-margin'},
//         {id:5, item:'"data": [', classbox:'padding-left-20px no-margin'},
//         {id:6, item: <span>&#123;</span>, classbox:'padding-left-40px no-margin'},
//         {id:7, item:'"name": "ANTONIO SERGIO RESENDE",', classbox:'padding-left-60px no-margin'},
//         {id:8, item:'"interpol": false,', classbox:'padding-left-60px no-margin'},
//         {id:9, item:'"ofac": false,', classbox:'padding-left-60px no-margin'},
//         {id:10, item:'"death": false,', classbox:'padding-left-60px no-margin'},
//         {id:11, item:'"pep": false,', classbox:'padding-left-60px no-margin'},
//         {id:12, item:'"pep_relative": false,', classbox:'padding-left-60px no-margin'},
//         {id:13, item:'"valid_id_card": true,', classbox:'padding-left-60px no-margin'},
//         {id:14, item:'"birth_date": "1974-01-19",', classbox:'padding-left-60px no-margin'},
//         {id:15, item:'"mothers_name": "MARIA VAZ RESENDE",', classbox:'padding-left-60px no-margin'},
//         {id:16, item:'"gender": "M",', classbox:'padding-left-60px no-margin'},
//         {id:17, item:'"rg": "0"', classbox:'padding-left-60px no-margin'},
//         {id:18, item: <span>&#125;</span>, classbox:'padding-left-40px no-margin'},
//         {id:19, item: <span>&#93;</span>, classbox:'padding-left-20px no-margin'},
//         {id:20, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // 404 - Not found (user not found)
//     const infobox3 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: "'code': 404,", classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: "'status': ‘Not found’,", classbox:'padding-left-20px no-margin'},  
//         {id:4, item: "'message': 'CPF not found'", classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // 409 - Internal server error
//     const infobox4 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: "'code': 409,", classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: "'status': ‘Fail',", classbox:'padding-left-20px no-margin'},  
//         {id:4, item: "'message': 'A query error has occurred'", classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     // 502 - Query error
//     const infobox5 = [
//         {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
//         {id:2, item: "'code': 502,", classbox:'padding-left-20px no-margin'}, 
//         {id:3, item: "'status': ‘error’,", classbox:'padding-left-20px no-margin'},  
//         {id:4, item: "'message': 'Internal error'", classbox:'padding-left-20px no-margin'},  
//         {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
//     ]

//     return (
//         <>
//         <Header/>
//         <main className="container"> 
//             <section className="container-text api">
//                 <p><Link to='/api'>Back to API Documentation</Link></p>

//                 <h1 className="padding-top-25px">Realtime Response</h1>

//                 <div className="break-text-api">
//                     <p>CPF is the Brazilian ID card for citizens. It’s an obligatory, unique number.</p>
    
//                     <p>This service queries the CPF number on the Brazil Federal Government database and returns the user's data, 
//                     if valid, the user's date of birth and other important information.</p>
//                 </div>              

//                 <div className="break-text-api">
//                     <p>When the user information is not on Brazil Federal Government database, please refer to <Link className="link-view-api" to='/api/asynchronousresponse'>Asynchronous Response.</Link></p>
//                 </div>

//                 <div className="break-text-api">
//                     <p className="blue">SANDBOX</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> https://sandbox.brainpay.com.br/user/cpf-research/&lt;cpf number&gt;</span></p>
//                 </div>

//                 <div className="break-text-api">
//                     <p className="blue">PRODUCTION</p> 
//                     <div className="line-api"></div>
//                     <p><span className="blue-no-strong"> https://api.brainpay.com.br/user/cpf-research/&lt;cpf number&gt;</span></p>
//                 </div>
                
//                 <p className="padding-bottom-25px">Method: GET</p>

//                 <p className="padding-top-25px">Payload</p>
//                 <Table columns={columns} data={data1}/>

//                 <p className="padding-top-25px">Response fields (success):</p>
//                 <Table columns={columns} data={data2}/>

//                 <p className="padding-top-25px">Data fields:</p>
//                 <Table columns={columns} data={data3}/>

//                 <p className="padding-top-25px">Curl example:</p>
//                 <BoxBlackApi infobox={infobox1}></BoxBlackApi>
                
//                 <p>Success response example:</p>
//                 <BoxBlackApi infobox={infobox2}></BoxBlackApi>

//                 <p>404 - Not found (user not found):</p>
//                 <BoxBlackApi infobox={infobox3}></BoxBlackApi>

//                 <p>409 - Internal server error:</p>
//                 <BoxBlackApi infobox={infobox4}></BoxBlackApi>
                
//                 <p>502 - Query error:</p>
//                 <BoxBlackApi infobox={infobox5}></BoxBlackApi>
                
//                 <p className="padding-top-25px"><Link to='/api'>Back to API Documentation</Link></p>
//             </section>
//         </main>
//         <Footer check={false}/>
//         </>
//     )
// }

// export default RealtimeResponse

