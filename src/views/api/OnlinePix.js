import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import BoxBlackApi from '../../components/BoxBlackApi'
import ImgOne from '../../assets/api/onlinepix-one.png'
import ImgTwo from '../../assets/api/onlinepix-two.png'


const OnlinePix = () => {
        // Black Box
        // Success Response
        const infobox2 = [
            {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
            {id:2, item: '"code": 200,', classbox:'padding-left-20px no-margin'},  
            {id:3, item: '"status": "Approved",', classbox:'padding-left-20px no-margin'}, 
            {id:4, item: '"transaction_id": "08d82909-f5d6-4437-b621-79260ee62f15",', classbox:'padding-left-20px no-margin'}, 
            {id:5, item: '"amount": "1200.00"', classbox:'padding-left-20px no-margin'}, 
            {id:6, item: <span>&#125;</span>, classbox:'no-margin'},
        ]

        // Not Found
        const infobox3 = [
            {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
            {id:2, item: '"status": "Not found",', classbox:'padding-left-20px no-margin'},  
            {id:3, item: '"code": 404,', classbox:'padding-left-20px no-margin'}, 
            {id:4, item: '"message": "Register not found"', classbox:'padding-left-20px no-margin'}, 
            {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
        ]

        // Unauthorized
        const infobox4 = [
            {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
            {id:2, item: '"code": 401,', classbox:'padding-left-20px no-margin'},  
            {id:3, item: '"status": "Unauthorized",', classbox:'padding-left-20px no-margin'}, 
            {id:4, item: <span>&#125;</span>, classbox:'no-margin'},
        ]

        // Internal server error
        const infobox5 = [
            {id:1, item: <span>&#123;</span>, classbox:'no-margin'},
            {id:2, item: '"code": 409,', classbox:'padding-left-20px no-margin'},  
            {id:3, item: '"status": "string",', classbox:'padding-left-20px no-margin'},
            {id:4, item: '"description": "string"', classbox:'padding-left-20px no-margin'}, 
            {id:5, item: <span>&#125;</span>, classbox:'no-margin'},
        ]

        // Curl example
        const infobox1 = [
            {id:1, item: <span>curl -X GET "http://sandbox.brainpay.com.br/recharge/4765759" -H "accept:</span>, classbox:'no-margin'},
            {id:2, item: <span>application/json" -H "Authorization:</span>, classbox:'no-margin'},
            {id:3, item: <span>eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2</span>, classbox:'no-margin'},
            {id:4, item: <span>MTQwNzY0MTQsImlhdCI6MTYxNDA3NTUxNCwic3ViIjozfQ.Uj</span>, classbox:'no-margin'},
            {id:5, item: <span>yec6v1kuCTL2RTqD6b9MrGN24mi4L9csB4yjKRyWY"</span>, classbox:'no-margin'},
        ]
        return(
            <>
                <Header/>
                <main className="container"> 
                    <section className="container-text api">
                        <p><Link to='/api'>Back to API Documentation</Link></p>
                        
                        <h1>PIX</h1>

                        <p>If you want to use the Brain Pay interface, use our API as below:</p>
                        
                        <div>
                            <p>With the fresh token generated in the previous step <Link className="link-view-api" to='/api/authentication'> Authentication in Production Environment</Link>, you 
                            have to take the user to the temporary link. In case of questions about generating token, refer to <Link className="link-view-api" to='/api/authentication'>Authentication in Production Environment.</Link></p>
                        </div>
                        
                        <div>
                            <p>This link will be available by concatenating the address of the environment with the amount that the 
                            user will pay and the token string generated in the previous step:</p>
                        </div>

                        <div className="break-text-api">
                            <p className="blue">SANDBOX</p> 
                            <div className="line-api"></div>
                            <p><span className="blue-no-strong">https://deposit-sandbox.brainpay.com.br/pix/brcode/1/</span>&lt;amount&gt;&lt;token&gt;</p>
                        </div>

                        <div className="break-text-api">
                            <p className="blue">PRODUCTION</p> 
                            <div className="line-api"></div>
                            <p><span className="blue-no-strong">https://deposit.brainpay.com.br/pix/brcode/1/</span>&lt;amount&gt;&lt;token&gt;</p>
                        </div>

                        <div>
                            <p>The Payment method "PIX" is ​​only available when using a fixed value on url (It has to be an integer value).
                            This fixed value is the amount the user will pay.</p>
                        </div>

                        <div>
                            <p>For instance, a temporary page generated from the amount of <span className="strong">R$120</span> would have a like that look like this:</p>
                        </div>

                        <div className="break-text-api">
                            <p className="blue">EXAMPLE</p> 
                            <div className="line-api"></div>
                            <p>Sandbox: https://deposit-sandbox.brainpay.com.br/pix/brcode/1/<span className="blue">120</span>/eyJ0eXA...</p>
                            <p>Production: https://deposit.brainpay.com.br/pix/brcode/1/<span className="blue">120</span>/eyJ0eXAiOiJK...</p>
                        </div>


                        <div>
                            <p>This page has a lifetime of 15 minutes. After that, it no longer can be found.</p>
                        </div>
                        
                        <h3>Pages</h3>

                        <p>The webpage generated by the link above would look like this:</p>

                        <div className="padding-left-40px">
                            <p> 1 - “Valor” (Amount) - Fixed value on url.</p>
                            <p>2 - "Nome" (Enter Name) - Must be a registered user.</p>
                            <p>3 - "CPF" (Enter CPF) - Must be a registered user.</p>
                            <p>In case of questions about registering the user, refer to <Link className="link-view-api" to='/api/cpfqueryservice'>CPF Query Service</Link> </p>
                        </div>

                        <img className="padding-bottom-30px" src={ImgOne} alt='PIX Our Interface'/>

                        <p>Upon clicking on "CONFIRMAR" the following screen will pop:</p>

                        <div className="padding-left-40px">
                            <p>The user will make a payment following the steps:</p>
                            <p>1 - “Valor” (Amount) - Fixed value on url and “Pague até” is the date to make the payment.</p>
                            <p>2 - To make the payment, scan the code below with your bank's PIX payment application.</p>
                            <p>3 - If you prefer, click on the button below to copy the PIX code and paste it into your bank's application.</p>
                        </div>

                        <div className="highlight-api">
                            <p>Important: The displaying of the qrcode in the payment page is optional. 
                            You can ask our team to hide it if you think it fits better to your business model showing only the pix 
                            code to be copied/pasted into the bank application.</p>
                        </div>

                        <img className="padding-bottom-30px" src={ImgTwo} alt='PIX Our Interface'/>
           
                        <h3>Retrieve a recharge status</h3>

                        <div className="highlight-api">
                            <p>Important: In order to avoid flooding our server with many requests from several 
                            companies, we strongly recommend not using this method as your main resource for 
                            getting a transaction status. Instead, use our Callback Methods.</p>
                        </div>

                        <div className="break-text-api">
                            <p className="blue">BRAINPAY API</p> 
                            <div className="line-api"></div>
                            <p><span className="strong">Sandbox:</span> http://sandbox.brainpay.com.br/recharge/<span className="blue">transaction_id</span></p>
                            <p><span className="strong">Production:</span> http://brainpay.com.br/recharge/<span className="blue">transaction_id</span></p>
                        </div>

                        <div className="break-text-api">
                            <p className="blue">EXAMPLE</p> 
                            <div className="line-api"></div>
                            <p><span className="strong">Sandbox:</span> http://sandbox.brainpay.com.br/recharge/<span className="blue">4765759</span></p>
                            <p><span className="strong">Production:</span>  http://brainpay.com.br/recharge/<span className="blue">4765759</span></p>
                        </div>

                        <p>Curl example:</p>
                        <BoxBlackApi infobox={infobox1}></BoxBlackApi>
            
                        <p>Success Response:</p>
                        <BoxBlackApi infobox={infobox2}></BoxBlackApi>

                        <p>Not found (user not found):</p>
                        <BoxBlackApi infobox={infobox3}></BoxBlackApi>

                        <p>Errors:</p>

                        <p>Unauthorized</p>
                        <BoxBlackApi infobox={infobox4}></BoxBlackApi>

                        <p>Internal server error</p>
                        <BoxBlackApi infobox={infobox5}></BoxBlackApi>

                        <p><Link to='/api'>Back to API Documentation</Link></p>
                    </section>
                </main>
                <Footer check={false} />
            </>
        )
}
export default OnlinePix