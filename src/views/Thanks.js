import React from 'react'
import { Link } from 'react-router-dom'
import {useTranslation} from "react-i18next"
import Header from '../components/Header'
import Footer from '../components/Footer'

const Erro = () => {

    const {t} = useTranslation()
    
    return (
        <>
        <Header/>
        <main className="container"> 
                <section className="thanks">
                    <div>
                        <i className="fas fa-paper-plane fa-5x"></i>
                    </div>
                    <div>
                        <h1>{t('thanks.thanks')}</h1>
                    </div>
                    <div>
                        <h2>{t('thanks.paragraph_1')}</h2>
                        <p>{t('thanks.paragraph_2')}<Link to='/'>BrainPay</Link></p>
                    </div>
                </section>
            </main>
        <Footer check={false}/>
        </>
    )
}

export default Erro