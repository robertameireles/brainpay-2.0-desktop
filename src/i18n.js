import i18n from 'i18next';
import { initReactI18next } from 'react-i18next' 

import translationEN from "./translations/en/translationEN.json"
import translationPT from "./translations/pt/translationPT.json"
import translationCH from "./translations/ch/translationCH.json"

// the translations
const resources = {
    en: {
        translation: translationEN              
    },
    pt: {
        translation: translationPT
    },
    ch: {
        translation: translationCH
    },
}

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        lng: 'en',

        interpolation: {
            escapeValue: false // react already safes from xss
        }
    })

export default i18n