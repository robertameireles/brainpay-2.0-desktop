import React from 'react';


function withdrawForm () {

    return (
        <>
        <h2>Form Example</h2>
        <div className="line"></div>
        <div className="withdrawform">
            <form>
                <label htmlFor="cpf"><span>01</span> CPF</label>
                    <input type="text" id="cpf" name="cpf"></input>
                <label htmlFor="name"><span>02</span> Nome Completo</label>
                    <input type="text" id="name" name="name"></input>
                <label htmlFor="value"><span>03</span> Valor</label>
                    <input type="text" id="value" name="value"></input>
                <label htmlFor="bank_number"><span>04</span> Banco</label>
                    <input type="text" id="bank_number" name="bank_number"></input>
                <label htmlFor="branch"><span>05</span> Número da Agência</label>
                    <input type="text" id="branch" name="branch"></input>

                <div className="withdrawform-flex">
                    <div>
                        <label htmlFor="account_number"><span>06</span> Número da Conta</label>
                            <input className="account-number" type="text" id="account_number" name="account_number"></input>
                    </div>
                    <div>
                        <label htmlFor="digit_code"><span>07</span> Dígito da Conta</label>
                            <input className="digit-code" type="text" id="digit_code" name="digit_code"></input> 
                    </div>
                </div>
                
                <label htmlFor="account_type"><span>08</span> Tipo da Conta</label>
                    <input type="text" id="account_type" name="account_type"></input>
            </form>
        </div>
        </>
    )
}

export default withdrawForm