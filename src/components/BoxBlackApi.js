import React from 'react'

function BoxBlackApi (props) {
    
    const text = props.infobox.map((infobox) => 
        <p key={infobox.id} className={infobox.classbox}>{infobox.item}</p> 
    )
    return (
        <div className="code-box-black-api">
            {text}
        </div>
    )
}

export default BoxBlackApi
