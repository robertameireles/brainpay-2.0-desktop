import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X POST "https://service.brainpay.com.br/test/v1/auth/company/account" 
-H "accept:application/json" 
-H "Content-Type: application/json" -d "{ \\"email\\":
\\"test@test.com\\", \\"password\\": \\"12345678\\"}"`

function CodeCreateUserCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserCurl
