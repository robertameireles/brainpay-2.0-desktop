import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeCreateUserCurl from './CodeCreateUserCurl'
import CodeCreateUserPHP from './CodeCreateUserPhp'
import CodeCreateUserNode from './CodeCreateUserNode'
import CodeCreateUserPython from './CodeCreateUserPython'
import CodeCreateUserJava from './CodeCreateUserJava'
import CodeCreateUserRuby from './CodeCreateUserRuby'


function TabsCreateUser () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeCreateUserCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeCreateUserPHP/>
                </TabPanel>
                <TabPanel>
                    <CodeCreateUserNode/>
                </TabPanel>
                <TabPanel>
                    <CodeCreateUserPython/>
                </TabPanel>
                <TabPanel>
                    <CodeCreateUserJava/>
                </TabPanel>
                <TabPanel>
                    <CodeCreateUserRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsCreateUser
