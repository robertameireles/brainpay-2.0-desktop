import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def call_api(headers,data):

    r = requests.post(
                    "https://service.brainpay.com.br/test/v1/auth/company/account",
                    data=json.dumps(data), headers=headers)

    data = json.loads(r.text) 

    print(data)


# VARIABLES
headers = {
    'content-type': 'application/json'
}

data = {
    'email': '<email>',
    'password': '<password>',
}

# CALL FUNCTION
if __name__ == '__main__':
    call_api(headers,data)`

function CodeCreateUserPython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserPython
