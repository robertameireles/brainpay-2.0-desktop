import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


class CallApi {
    
public static void main(String[] args) 
throws IOException, InterruptedException {

    HttpClient client = HttpClient.newBuilder().build();
    HttpRequest request = HttpRequest.newBuilder(URI.create(
        "https://service.brainpay.com.br/test/v1/auth/company/login"))
        .header("Content-Type", "application/json")
        .POST(HttpRequest.BodyPublishers.ofString(
        "{\\"email\\": \\"<email>\\",\\"password\\": 
        \\"<password>\\"}")).build();

    HttpResponse<String> response = client.send(
        request,HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

}

}`

function CodeGetTokenJava (props) {

    return (
        <SyntaxHighlighter language="java" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeGetTokenJava
