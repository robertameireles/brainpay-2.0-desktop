import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def CallAPI(headers,data):

    r = requests.post("
                    https://service.brainpay.com.br/test/v1/auth/company/login",
                    data=json.dumps(data), headers=headers)

    data = json.loads(r.text) 

    print(r.text)


# VARIABLES
headers = {
    'content-type': 'application/json'
}

data = {
    'email': '<email>',
    'password': '<password>',
}

# CALL FUNCTION
if __name__ == '__main__':
    CallAPI(headers,data)`

function CodeGetTokenPython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeGetTokenPython
