import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeGetTokenCurl from './CodeGetTokenCurl'
import CodeGetTokenPHP from './CodeGetTokenPhp'
import CodeGetTokenNode from './CodeGetTokenNode'
import CodeGetTokenPython from './CodeGetTokenPython'
import CodeGetTokenJava from './CodeGetTokenJava'
import CodeGetTokenRuby from './CodeGetTokenRuby'


function TabsGetToken () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeGetTokenCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeGetTokenPHP/>
                </TabPanel>
                <TabPanel>
                    <CodeGetTokenNode/>
                </TabPanel>
                <TabPanel>
                    <CodeGetTokenPython/>
                </TabPanel>
                <TabPanel>
                    <CodeGetTokenJava/>
                </TabPanel>
                <TabPanel>
                    <CodeGetTokenRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsGetToken
