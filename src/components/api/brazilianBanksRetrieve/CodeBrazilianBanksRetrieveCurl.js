import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X GET "https://service.brainpay.com.br/test/v1/withdrawal/banks" 
-H "accept:application/json" -H "Authorization: Bearer
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Nzk2MjQ4MTAsImlhdCI6MTU3
OTYyMzkxMCwic3ViIjozfQ.aWbGVYIu__yvOi_DwNqhAPvqcLvZxr3nevJknzpnNYE"`

function CodeBrazilianBanksRetrieveCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeBrazilianBanksRetrieveCurl

