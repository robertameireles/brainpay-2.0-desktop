import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def call_api(uri,headers):

    r = requests.get(uri,headers=headers)

    response = json.loads(r.text) 

    print(response)


# VARIABLES
uri = "https://service.brainpay.com.br/test/v1/withdrawal/banks"

headers = {
    'content-type': 'application/json',
    'Authorization':'Bearer <token>'
}

# CALL FUNCTION
if __name__ == '__main__':
    call_api(uri,headers)
`

function CodeBrazilianBanksRetrievePython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeBrazilianBanksRetrievePython
