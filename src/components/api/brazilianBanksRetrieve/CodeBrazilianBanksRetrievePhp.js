import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `<?php
// FUNCTION

function CallAPI($uri, $header){

    $curl = curl_init();
    
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $uri);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

// VARIABLES
$header = array(
    'Content-Type:application/json',
    'Authorization:Bearer <token>'
    );  

$uri = "https://service.brainpay.com.br/test/v1/withdrawal/banks";

// CALL FUNCTION
$result = CallAPI($uri, $header);

// RTESULT
echo $result;

?>`

function CodeBrazilianBanksRetrievePhp (props) {

    return (
        <SyntaxHighlighter language="php" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeBrazilianBanksRetrievePhp
