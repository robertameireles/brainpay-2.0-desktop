import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeBrazilianBanksRetrieveCurl from './CodeBrazilianBanksRetrieveCurl'
import CodeBrazilianBanksRetrievePHP from './CodeBrazilianBanksRetrievePhp'
import CodeBrazilianBanksRetrieveNode from './CodeBrazilianBanksRetrieveNode'
import CodeBrazilianBanksRetrievePython from './CodeBrazilianBanksRetrievePython'
import CodeBrazilianBanksRetrieveJava from './CodeBrazilianBanksRetrieveJava'
import CodeBrazilianBanksRetrieveRuby from './CodeBrazilianBanksRetrieveRuby'


function TabsCodeBrazilianBanksRetrieve () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeBrazilianBanksRetrieveCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeBrazilianBanksRetrievePHP/>
                </TabPanel>
                <TabPanel>
                    <CodeBrazilianBanksRetrieveNode/>
                </TabPanel>
                <TabPanel>
                    <CodeBrazilianBanksRetrievePython/>
                </TabPanel>
                <TabPanel>
                    <CodeBrazilianBanksRetrieveJava/>
                </TabPanel>
                <TabPanel>
                    <CodeBrazilianBanksRetrieveRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsCodeBrazilianBanksRetrieve
