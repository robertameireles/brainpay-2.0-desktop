import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `const fetch = require('node-fetch')

// FUNCTION
function CallAPI (uri, method, headers){
        const resp = fetch(uri, {
        method: method,
        headers: headers
    }).then(res => res.json())
    .then(json => console.log(json));

    console.log(resp)
}

// VARIABLES
const uri = 'https://service.brainpay.com.br/test/v1/withdrawal/banks'

const method = 'GET'

const headers = {
    'Content-Type': 'application/json',
    'Authorization':'Bearer <token>'
};

// CALL FUNCTION
CallAPI(uri, method, headers)`

function CodeBrazilianBanksRetrieveNode (props) {
    return (
        <SyntaxHighlighter language="javascript" style={darcula}>
          {code}
        </SyntaxHighlighter>
    )
}

export default CodeBrazilianBanksRetrieveNode



