import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X 'POST' 'https://service.brainpay.com.br/test/v1/deposit/status'   
-H 'accept: application/json' -H 'Authorization: Baerer eyJ0eXAiOiJK
V1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjEwMTQ2MjcsImlhdCI6MTYyMTAxMzcyN
ywic3ViIjoxNTJ9.lEXgY8TT_fjOGfPkbiBbAtNbYxawx5snfuHrUGo27XM'   
-H 'Content-Type: application/json'   
-d '{
  "data": [
    "/6786136736361/",
    "/838791629616/",
    "/32696196343314/"
  ]
}'`

function CodeRechargeInformationRetrieveCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeRechargeInformationRetrieveCurl
