import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `const fetch = require('node-fetch')

// FUNCTION
function CallAPI (url, method, headers, body){
        const resp = fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
    }).then(res => res.json())
    .then(json => console.log(json));

    console.log(resp)
}

// VARIABLES
const url = 'https://service.brainpay.com.br/test/v1/deposit/status'

const method = 'POST'

const headers = {
    'Authorization':'Bearer <token>',
    'Content-Type': 'application/json'
};


const body = {
    data: ['<id_transaction>', '<id_transaction>', '<id_transaction>']
};

// CALL FUNCTION
CallAPI(url, method, headers, body)`

function CodeRechargeInformationRetrieveNode (props) {
    return (
        <SyntaxHighlighter language="javascript" style={darcula}>
          {code}
        </SyntaxHighlighter>
    )
}

export default CodeRechargeInformationRetrieveNode



