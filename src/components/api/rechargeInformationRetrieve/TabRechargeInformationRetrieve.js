import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeApiRechargeInformationRetrieveCurl from './CodeRechargeInformationRetrieveCurl'
import CodeApiRechargeInformationRetrievePHP from './CodeRechargeInformationRetrievePhp'
import CodeApiRechargeInformationRetrieveNode from './CodeRechargeInformationRetrieveNode'
import CodeApiRechargeInformationRetrievePython from './CodeRechargeInformationRetrievePython'
import CodeApiRechargeInformationRetrieveJava from './CodeRechargeInformationRetrieveJava'
// import CodeApiRechargeInformationRetrieveRuby from './CodeRechargeInformationRetrieveRuby'


function TabRechargeInformationRetrieve () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    {/* <Tab>RUBY</Tab> */}
                </TabList>

                <TabPanel>
                    <CodeApiRechargeInformationRetrieveCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeApiRechargeInformationRetrievePHP/>
                </TabPanel>
                <TabPanel>
                    <CodeApiRechargeInformationRetrieveNode/>
                </TabPanel>
                <TabPanel>
                    <CodeApiRechargeInformationRetrievePython/>
                </TabPanel>
                <TabPanel>
                    <CodeApiRechargeInformationRetrieveJava/>
                </TabPanel>
                {/* <TabPanel>
                    <CodeApiRechargeInformationRetrieveRuby/>
                </TabPanel> */}
            </Tabs>
        </>
    )
}

export default TabRechargeInformationRetrieve
