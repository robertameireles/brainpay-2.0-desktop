import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X 'POST'
  'https://service.brainpay.com.br/test/v1/withdrawal/ted'
  -H 'accept: application/json'
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjEwMTQzNj
  EsImlhdCI6MTYyMTAxMzQ2MSwic3ViIjoxNTJ9.FOJfoMdkqwLlpz7t-r2Bs1mumrnVYZxlKSq_lRiuU-0'
  -H 'Content-Type: application/json'
  -d '{
  "idCard": "39189183002",
  "name": "José da Silva",
  "amount": 1000,
  "bankNumber": "001",
  "branch": 122,
  "accountNumber": 3582885,
  "digitCode": "05",
  "accountType": 1
}'`

function CodeCreateUserCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserCurl
