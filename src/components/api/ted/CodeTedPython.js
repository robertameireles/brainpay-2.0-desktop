import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def call_api(url,headers,data):

    r = requests.post(url,
                    data=json.dumps(data), headers=headers)

    response = json.loads(r.text) 

    print(response)
    
    
# VARIABLES
url = "https://service.brainpay.com.br/test/v1/withdrawal/ted"

headers = {
    'content-type': 'application/json',
    'Authorization':'Bearer <token>'

}

data = {
    "idCard": "<idCard>",
    "name": "<name>",
    "amount": <amount>,
    "bankNumber": "<bankNumber>",
    "branch": <branch>,
    "accountNumber": <accountNumber>,
    "digitCode": "<digitCode>",
    "accountType": <accountType>
}


# CALL FUNCTION
if __name__ == '__main__':
    call_api(url,headers,data)`

function CodeCreateUserPython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserPython
