import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeTedCurl from './CodeTedCurl'
import CodeTedPHP from './CodeTedPhp'
import CodeTedNode from './CodeTedNode'
import CodeTedPython from './CodeTedPython'
import CodeTedJava from './CodeTedJava'
import CodeTedRuby from './CodeTedRuby'


function TabsCreateUser () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeTedCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeTedPHP/>
                </TabPanel>
                <TabPanel>
                    <CodeTedNode/>
                </TabPanel>
                <TabPanel>
                    <CodeTedPython/>
                </TabPanel>
                <TabPanel>
                    <CodeTedJava/>
                </TabPanel>
                <TabPanel>
                    <CodeTedRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsCreateUser
