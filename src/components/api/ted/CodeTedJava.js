import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

// FUNCTION
class CallApi {
    
public static void main(String[] args) throws IOException, InterruptedException {

    HttpClient client = HttpClient.newBuilder().build();
    HttpRequest request = HttpRequest.newBuilder(URI.create(
        "https://service.brainpay.com.br/test/v1/withdrawal/ted"))
        .header("Content-Type", "application/json")
        .header("Authorization","Bearer <token>")
        .POST(HttpRequest.BodyPublishers.ofString(
        "{\"idCard\": \"<idCard>\",
        \"name\": \"<name>\",
        \"amount\": <amount>,
        \"bankNumber\": \"<bankNumber>\",
        \"branch\": <branch>, 
        \"accountNumber\": <accountNumber>,
        \"digitCode\": \"<digitCode>\",
        \"accountType\": <accountType>}"))
        .build();

    HttpResponse<String> response = client.send(request,HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());

}

}`

function CodeCreateUserJava (props) {

    return (
        <SyntaxHighlighter language="java" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserJava
