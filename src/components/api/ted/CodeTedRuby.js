import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `require 'uri'
require 'net/http'
require 'json'

# VARIABLES
uri =  URI("https://service.brainpay.com.br/test/v1/withdrawal/ted")

headers = {'Content-Type' => 'application/json',
        'Authorization'=>'Baerer <token>'
    }

data = {
  "idCard": "<idCard>",
  "name": "<name>",
  "amount": <amount>,
  "bankNumber": "<bankNumber>",
  "branch": <branch>,
  "accountNumber": <accountNumber>,
  "digitCode": "<digitCode>",
  "accountType": <accountType>
}

# FUNCTION
def call_api(uri,headers,data)
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    response = https.post(uri.path, data.to_json, headers) 
    puts response.body
end

# CALL FUNCTION
call_api(uri,headers,data)`

function CodeCreateUserRuby (props) {

    return (
        <SyntaxHighlighter language="ruby" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeCreateUserRuby
