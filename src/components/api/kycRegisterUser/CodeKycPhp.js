import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `<?php
// FUNCTION
function CallAPI($url, $header, $cpfNumber){

    $uri = $url. $cpfNumber;
    $curl = curl_init();
    
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $uri);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

// VARIABLES
$header = array(
    'Content-Type:application/json',
    'Authorization:Bearer <token>'
    );  

$url = "https://service.brainpay.com.br/test/v1/kyc/cpf";

$cpfNumber = '<cpf_number>';


// CALL FUNCTION
$result = CallAPI($url, $header, $cpfNumber);

// RESULT
echo $result;

?>`

function CodeKycPhp (props) {

    return (
        <SyntaxHighlighter language="php" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycPhp
