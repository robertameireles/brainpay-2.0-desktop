import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeKycCurl from './CodeKycCurl'
import CodeKycPHP from './CodeKycPhp'
import CodekycNode from './CodeKycNode'
import CodeKycPython from './CodeKycPython'
import CodeKycJava from './CodeKycJava'
import CodeKycRuby from './CodeKycRuby'


function TabsKyc () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeKycCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeKycPHP/>
                </TabPanel>
                <TabPanel>
                    <CodekycNode/>
                </TabPanel>
                <TabPanel>
                    <CodeKycPython/>
                </TabPanel>
                <TabPanel>
                    <CodeKycJava/>
                </TabPanel>
                <TabPanel>
                    <CodeKycRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsKyc
