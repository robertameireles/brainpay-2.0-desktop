import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `require 'uri'
require 'net/http'
require 'json'

# VARIABLES
url = URI("https://service.brainpay.com.br/test/v1/kyc/cpf/")

headers = {'Content-Type' => 'application/json', 
        'Authorization'=>'Bearer <token>', 
        }

data = '<cpfNumber>'

# FUNCTION
def call_api(url,headers,data)

    uri = url + data

    Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
        request = Net::HTTP::Get.new uri
        request['Content-Type'] =  headers['Content-Type']
        request['Authorization'] = headers['Authorization']
        response = http.request request # Net::HTTPResponse object

        puts response.body

    end

end

# CALL FUNCTION
call_api(url,headers,data)`

function CodeKycRuby (props) {

    return (
        <SyntaxHighlighter language="ruby" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycRuby
