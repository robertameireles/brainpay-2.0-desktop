import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def CallAPI(url,headers,data):

    r = requests.post(url,
                    data=json.dumps(data), headers=headers)

    data = json.loads(r.text) 

    print(data)


# VARIABLES
url = 'https://deposit-sandbox.brainpay.com.br/jwt/access/'

headers = {
    'content-type': 'application/json',
    'Authorization':'Basic <base64(user:password)>'
}

data = {
    'user_id': '<user CPF number>'
}

# CALL FUNCTION
if __name__ == '__main__':
    CallAPI(url,headers,data)
`

function CodeAuthenticationPython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeAuthenticationPython
