import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X POST "https://deposit-sandbox.brainpay.com.br/jwt/access/" 
-H "accept: application/json" -H "Content-Type: application/json" 
-H "Authorization: Basic am9obmRvZUBlbWFpbC5jb206MTIzNDU2" -d 
"{ \"user_id\": \"12435301898\"}"`

function CodeAuthenticationCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeAuthenticationCurl

