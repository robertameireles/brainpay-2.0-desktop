import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeAuthenticationCurl from './CodeAuthenticationCurl'
import CodeAuthenticationPHP from './CodeAuthenticationPhp'
import CodeAuthenticationNode from './CodenAuthenticatioNode'
import CodeAuthenticationPython from './CodeAuthenticationPython'
import CodeAuthenticationJava from './CodeAuthenticationJava'
// import CodeAuthenticationRuby from './CodeAuthenticationRuby'


function TabsAuthentication () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    {/* <Tab>RUBY</Tab> */}
                </TabList>

                <TabPanel>
                    <CodeAuthenticationCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeAuthenticationPHP/>
                </TabPanel>
                <TabPanel>
                    <CodeAuthenticationNode/>
                </TabPanel>
                <TabPanel>
                    <CodeAuthenticationPython/>
                </TabPanel>
                <TabPanel>
                    <CodeAuthenticationJava/>
                </TabPanel>
                {/* <TabPanel>
                    <CodeAuthenticationRuby/>
                </TabPanel> */}
            </Tabs>
        </>
    )
}

export default TabsAuthentication
