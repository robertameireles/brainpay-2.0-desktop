import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `<?php
// FUNCTION
function CallAPI($method, $url, $header, $data = false)
{
    $curl = curl_init();

    $body = json_encode($data);

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

// VARIABLES
$header = array(
    'Content-Type:application/json',
    'Authorization: <Basic base64(user:password)>'
    ); 

$data = array(
    'user_id' => '<user CPF number>',
    );  

$url = "https://deposit-sandbox.brainpay.com.br/jwt/access/";

$method = 'POST';

// CALL FUNCTION
$result = CallAPI($method, $url, $header, $data);

// RTESULT
echo $result;

?>`

function CodeAuthenticationPhp (props) {

    return (
        <SyntaxHighlighter language="php" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeAuthenticationPhp
