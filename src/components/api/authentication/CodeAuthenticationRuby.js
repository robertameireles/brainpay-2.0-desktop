import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `require 'uri'
require 'net/http'
require 'net/https'
require 'json'

# FUNCTION
def call_api(url,headers,data)
  http = Net::HTTP.new(url.host)
  resp = http.post(url.path, data.to_json, headers)
  puts resp.body

end

# VARIABLES
headers = {
    'content-type'=> 'application/json',
    'Authorization'=>'Basic <base64(user:password)>'
}

data = {'user_id' => '<user CPF number>'}

url = URI("https://deposit-sandbox.brainpay.com.br/jwt/access/")

# CALL FUNCTION
call_api(url,headers,data)

`

function CodeAuthenticationRuby (props) {

    return (
        <SyntaxHighlighter language="ruby" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeAuthenticationRuby
