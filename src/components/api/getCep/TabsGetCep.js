import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeGetCepCurl from './CodeGetCepCurl'
import CodeGetCepPHP from './CodeGetCepPhp'
import CodeGetCepNode from './CodeGetCepNode'
import CodeGetCepPython from './CodeGetCepPython'
import CodeGetCepJava from './CodeGetCepJava'
import CodeGetCepRuby from './CodeGetCepRuby'


function TabsKyc () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    <Tab>RUBY</Tab>
                </TabList>

                <TabPanel>
                    <CodeGetCepCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeGetCepPHP/>
                </TabPanel>
                <TabPanel>
                    <CodeGetCepNode/>
                </TabPanel>
                <TabPanel>
                    <CodeGetCepPython/>
                </TabPanel>
                <TabPanel>
                    <CodeGetCepJava/>
                </TabPanel>
                <TabPanel>
                    <CodeGetCepRuby/>
                </TabPanel>
            </Tabs>
        </>
    )
}

export default TabsKyc
