import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def call_api(url,headers,data):

    uri = f'{url}{data}'

    r = requests.get(uri,
                    headers=headers)

    response = json.loads(r.text) 

    print(response)


# VARIABLES
url = "https://service.brainpay.com.br/test/v1/kyc/cep/"

headers = {
    'content-type': 'application/json',
    'Authorization':'Bearer <token>'
}

data = '<zipCode>'

# CALL FUNCTION
if __name__ == '__main__':
    call_api(url,headers,data)
`

function CodeKycPython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycPython
