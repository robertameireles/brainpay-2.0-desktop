import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X GET "https://service.brainpay.com.br/test/v1/kyc/cep/05838970" 
-H "accept:application/json" -H "Content-Type: application/json" 
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.
eyJleHAiOjE2MTc5ODcxOTMsImlhdCI6MTYxNzk4NjI5Mywic3ViIjoz
fQ.nst7dSt24z7Er1iWePzxw_Oo4JgVHM9WJ9vYXJnj0o4"`

function CodeKycCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycCurl
