import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `<?php
// FUNCTION
    function CallAPI($url, $header, $data){

        $uri = $url. $data;
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $uri);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }

    // VARIABLES
    $header = array(
        'Content-Type:application/json',
        'Authorization:Bearer <token>'
        );  

    // Sets the URL
    $url = "https://service.brainpay.com.br/test/v1/kyc/cep/";


    // Sets the cpf_number
    $data = '<zipCode>';

    // CALL FUNCTION
    $result = CallAPI($url, $header, $data);
    
    // RTESULT
    echo $result;

?>`

function CodeKycPhp (props) {

    return (
        <SyntaxHighlighter language="php" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycPhp
