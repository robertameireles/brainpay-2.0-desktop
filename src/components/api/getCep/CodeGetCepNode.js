import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `const fetch = require('node-fetch');

// FUNCTION
    function CallAPI (url, method, headers, data){
            uri = url + data
            const resp = fetch(uri, {
            method: method,
            headers: headers
        }).then(res => res.json())
        .then(json => console.log(json));
    
        console.log(resp)
    }
    
    // VARIABLES
    const url = 'https://service.brainpay.com.br/test/v1/kyc/cep/'
    
    const method = 'GET'
    
    const data = '<zipCode>'
    
    const headers = {
        'Content-Type': 'application/json',
        'Authorization':'Bearer <token>'
    };
    
    // CALL FUNCTION
    CallAPI(url, method, headers, data)
`

function CodeKycNode (props) {
    return (
        <SyntaxHighlighter language="javascript" style={darcula}>
          {code}
        </SyntaxHighlighter>
    )
}

export default CodeKycNode



