import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import CodeWithdrawalTransactionRetrieveCurl from './CodeWithdrawalTransactionRetrieveCurl'
import CodeWithdrawalTransactionRetrievePHP from './CodeWithdrawalTransactionRetrievePhp'
import CodeWithdrawalTransactionRetrieveNode from './CodeWithdrawalTransactionRetrieveNode'
import CodeWithdrawalTransactionRetrievePython from './CodeWithdrawalTransactionRetrievePython'
import CodeWithdrawalTransactionRetrieveJava from './CodeWithdrawalTransactionRetrieveJava'
// import CodeWithdrawalTransactionRetrieveRuby from './CodeWithdrawalTransactionRetrieveRuby'


function ApiTabsWithdrawalRequestService () {
    
    return (
        <>
        <h2>Languages</h2>
            <Tabs>
                <TabList>
                    <Tab>CURL </Tab>  
                    <Tab>PHP</Tab>
                    <Tab>NODEJS</Tab>
                    <Tab>PYTHON</Tab>
                    <Tab>JAVA</Tab>
                    {/* <Tab>RUBY</Tab> */}
                </TabList>

                <TabPanel>
                    <CodeWithdrawalTransactionRetrieveCurl/>
                </TabPanel>
                <TabPanel>
                    <CodeWithdrawalTransactionRetrievePHP/>
                </TabPanel>
                <TabPanel>
                    <CodeWithdrawalTransactionRetrieveNode/>
                </TabPanel>
                <TabPanel>
                    <CodeWithdrawalTransactionRetrievePython/>
                </TabPanel>
                <TabPanel>
                    <CodeWithdrawalTransactionRetrieveJava/>
                </TabPanel>
                {/* <TabPanel>
                    <CodeWithdrawalTransactionRetrieveRuby/>
                </TabPanel> */}
            </Tabs>
        </>
    )
}

export default ApiTabsWithdrawalRequestService
