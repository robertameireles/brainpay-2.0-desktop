import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `const fetch = require('node-fetch')

// FUNCTION
function CallAPI (url, method, headers, transactionId){
        uri = url + transactionId
        const resp = fetch(uri, {
        method: method,
        headers: headers
    }).then(res => res.json())
    .then(json => console.log(json));

    console.log(resp)
}

// VARIABLES
const url = 'https://sandbox.brainpay.com.br/withdrawal/retrieve/'

const method = 'GET'

const transactionId = '<transaction_id>'

const headers = {
    'Content-Type': 'application/json',
    'Authorization':'Bearer <token>'
};

// CALL FUNCTION
CallAPI(url, method, headers, transactionId)
`

function CodeWithdrawalTransactionRetrieveNode (props) {
    return (
        <SyntaxHighlighter language="javascript" style={darcula}>
          {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrieveNode



