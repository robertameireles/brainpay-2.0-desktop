import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `<?php
// FUNCTION
function CallAPI($url, $header, $transactionId){

    $uri = $url. $transactionId;
    $curl = curl_init();
    
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_URL, $uri);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

// VARIABLES
$header = array(
    'Content-Type:application/json',
    'Authorization:Bearer <token>'
    );  

$url = "https://sandbox.brainpay.com.br/withdrawal/retrieve/​​";

$transactionId = '<transaction_id>';

// CALL FUNCTION
$result = CallAPI($url, $header, $transactionId);

// RTESULT
echo $result;

?>`

function CodeWithdrawalTransactionRetrievePhp (props) {

    return (
        <SyntaxHighlighter language="php" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrievePhp
