import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'

const code = `
curl -X GET 
"http://withdrawal.test.brainpay.com.br/withdrawal/retrieve/12345678" 
-H "accept: application/json" -H "Content-Type: application/json" 
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiO
jE1Nzk1MzEzMTUsImlhdCI6MTU3OTUzMDQxNSwic3ViIjozfQ.q-Jmn0j_41rxScTPKBr
YxogPXGNHUehZT7tbgfioIWM"`

function CodeWithdrawalTransactionRetrieveCurl (props) {

    return (
        <SyntaxHighlighter language="curl" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrieveCurl
