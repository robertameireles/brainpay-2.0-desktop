import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `require 'uri'
require 'net/http'
require 'net/https'
require 'json'

# FUNCTION
def call_api(url,headers, transaction_id)

    complete_url = url + transaction_id

    uri = URI(complete_url)
    req = Net::HTTP::Get.new(uri)
    req['Content-Type'] =  headers['Content-Type']
    req['Authorization'] = headers['Authorization']

    res = Net::HTTP.start(uri.hostname) {|http|
        http.request(req)
    }
    
    puts res.body
end

# VARIABLES
headers = {'Content-Type' => 'application/json', 
        'Authorization'=>'Bearer <token>', 
        }

transaction_id = '<transaction_id>'

url = URI("https://sandbox.brainpay.com.br/withdrawal/retrieve/")

# CALL FUNCTION
call_api(url,headers, transaction_id)
`

function CodeWithdrawalTransactionRetrieveRuby (props) {

    return (
        <SyntaxHighlighter language="ruby" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrieveRuby
