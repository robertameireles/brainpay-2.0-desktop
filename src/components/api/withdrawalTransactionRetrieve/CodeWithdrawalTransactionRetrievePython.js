import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import json, requests

# FUNCTION
def CallAPI(url,headers,transaction_id):

    uri = f'{url}{transaction_id}'

    r = requests.get(uri,headers=headers)

    response = json.loads(r.text) 

    print(response)

# VARIABLES
url = "https://sandbox.brainpay.com.br/withdrawal/retrieve/​​"

headers = {
    'content-type': 'application/json',
    'Authorization':'Bearer <token>'
}

transaction_id = 'transaction_id'

# CALL FUNCTION
if __name__ == '__main__':
    CallAPI(url,headers,transaction_id)
`

function CodeWithdrawalTransactionRetrievePython (props) {

    return (
        <SyntaxHighlighter language="python" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrievePython
