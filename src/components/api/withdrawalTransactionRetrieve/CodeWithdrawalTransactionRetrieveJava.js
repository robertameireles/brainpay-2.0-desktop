import React from "react"
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { darcula } from 'react-syntax-highlighter/dist/esm/styles/prism'


const code = `import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

class CallApi {
    
 public static void main(String[] args) throws IOException, 
 InterruptedException {
  
  HttpClient client = HttpClient.newHttpClient();
  HttpRequest request = HttpRequest.newBuilder()
    .uri(URI.create(
    "https://sandbox.brainpay.com.br/withdrawal/retrieve/<transaction_id>"))
    .header("Content-Type","application/json")
    .header("Authorization","Basic <token>")
    .build();
  
  HttpResponse<String> response = client.send(
  request,HttpResponse.BodyHandlers.ofString());
  System.out.println(response.body());

}
  
}
`

function CodeWithdrawalTransactionRetrieveJava (props) {

    return (
        <SyntaxHighlighter language="java" style={darcula}>
        {code}
        </SyntaxHighlighter>
    )
}

export default CodeWithdrawalTransactionRetrieveJava
